CREATE ROLE [aspnet_Membership_BasicAccess]
AUTHORIZATION [admin]
GO
EXEC sp_addrolemember N'aspnet_Membership_BasicAccess', N'aspnet_Membership_FullAccess'
GO
