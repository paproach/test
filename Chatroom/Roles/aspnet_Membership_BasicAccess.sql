CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [admin]
GO

ALTER ROLE [aspnet_Membership_BasicAccess] ADD MEMBER [aspnet_Membership_FullAccess]

GO
