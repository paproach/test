SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemberIDDisclosurePolicyOptions] (
		[MemberIDDisclosurePolicyID]     [int] NOT NULL,
		[MemberIDDisclosurePolicy]       [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblMemberIDDisclosurePolicy]
		PRIMARY KEY
		CLUSTERED
		([MemberIDDisclosurePolicyID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MemberIDDisclosurePolicyOptions] SET (LOCK_ESCALATION = TABLE)
GO
