SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserBuddies] (
		[UserID]          [int] NOT NULL,
		[BuddyUserID]     [int] NOT NULL,
		CONSTRAINT [PK_UserBuddies]
		PRIMARY KEY
		CLUSTERED
		([UserID], [BuddyUserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserBuddies]
	WITH CHECK
	ADD CONSTRAINT [FK_UserBuddies_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserBuddies]
	CHECK CONSTRAINT [FK_UserBuddies_Users]

GO
ALTER TABLE [dbo].[UserBuddies] SET (LOCK_ESCALATION = TABLE)
GO
