SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CourseActionPlans] (
		[ActionPlanID]     [int] NOT NULL,
		[CourseID]         [int] NOT NULL,
		[JoinDate]         [datetime] NULL,
		CONSTRAINT [PK_SpecialForceActionPlans]
		PRIMARY KEY
		CLUSTERED
		([ActionPlanID], [CourseID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CourseActionPlans]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceActionPlans_SpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[CourseActionPlans]
	CHECK CONSTRAINT [FK_SpecialForceActionPlans_SpecialForces]

GO
ALTER TABLE [dbo].[CourseActionPlans] SET (LOCK_ESCALATION = TABLE)
GO
