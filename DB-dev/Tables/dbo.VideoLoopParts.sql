SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[VideoLoopParts] (
		[LoopID]                [int] NOT NULL,
		[LoopElementTypeID]     [int] NULL,
		[DurationInMinutes]     [int] NOT NULL,
		CONSTRAINT [PK_VideoLoopParts]
		PRIMARY KEY
		CLUSTERED
		([LoopID], [DurationInMinutes])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoLoopParts] SET (LOCK_ESCALATION = TABLE)
GO
