SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sponsors] (
		[SponsorID]              [int] IDENTITY(1, 1) NOT NULL,
		[SponsorName]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[SponsorLogo]            [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[SponsorLink]            [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[SponsorshipDueDate]     [datetime] NULL,
		[ContactPerson]          [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[PhoneNumber]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Email]                  [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblSponsors]
		PRIMARY KEY
		CLUSTERED
		([SponsorID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sponsors] SET (LOCK_ESCALATION = TABLE)
GO
