SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInput] (
		[UserInputID]     [int] IDENTITY(1, 1) NOT NULL,
		[UserID]          [int] NULL,
		[Input]           [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Date]            [datetime] NULL,
		CONSTRAINT [PK_UserInput]
		PRIMARY KEY
		CLUSTERED
		([UserInputID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserInput] SET (LOCK_ESCALATION = TABLE)
GO
