SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[VideoChannelEPGs] (
		[VideoChannelID]        [int] NOT NULL,
		[Timestamp]             [datetime] NOT NULL,
		[VideoClipID]           [int] NULL,
		[DurationInMinutes]     [int] NULL,
		CONSTRAINT [PK_VideoChannelEPG]
		PRIMARY KEY
		CLUSTERED
		([VideoChannelID], [Timestamp])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoChannelEPGs] SET (LOCK_ESCALATION = TABLE)
GO
