SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementBookmark] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[UserID]            [int] NOT NULL,
		[Date]              [datetime] NULL,
		CONSTRAINT [PK_ElementBookmark]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementBookmark]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementBookmark_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementBookmark]
	CHECK CONSTRAINT [FK_ElementBookmark_ElementTypes]

GO
ALTER TABLE [dbo].[ElementBookmark]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementBookmark_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ElementBookmark]
	CHECK CONSTRAINT [FK_ElementBookmark_Users]

GO
ALTER TABLE [dbo].[ElementBookmark] SET (LOCK_ESCALATION = TABLE)
GO
