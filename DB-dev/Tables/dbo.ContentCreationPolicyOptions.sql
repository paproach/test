SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentCreationPolicyOptions] (
		[ContentAuthorizationPolicyID]     [int] NOT NULL,
		[ContentAuthorizationPolicy]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblContentAuthorizationPolicy]
		PRIMARY KEY
		CLUSTERED
		([ContentAuthorizationPolicyID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentCreationPolicyOptions] SET (LOCK_ESCALATION = TABLE)
GO
