SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ContentSurveyQuestionVotes] (
		[SurveyQuestionID]     [int] NOT NULL,
		[UserID]               [int] NOT NULL,
		[Answer]               [int] NULL,
		CONSTRAINT [PK_TblContentSurveyQuestionVotes]
		PRIMARY KEY
		CLUSTERED
		([UserID], [SurveyQuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentSurveyQuestionVotes]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentSurveyQuestionVotes_ContentSurveyQuestions]
	FOREIGN KEY ([SurveyQuestionID]) REFERENCES [dbo].[ContentSurveyQuestions] ([SurveyQuestionID])
ALTER TABLE [dbo].[ContentSurveyQuestionVotes]
	CHECK CONSTRAINT [FK_ContentSurveyQuestionVotes_ContentSurveyQuestions]

GO
ALTER TABLE [dbo].[ContentSurveyQuestionVotes]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentSurveyQuestionVotes_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ContentSurveyQuestionVotes]
	CHECK CONSTRAINT [FK_ContentSurveyQuestionVotes_Users]

GO
ALTER TABLE [dbo].[ContentSurveyQuestionVotes] SET (LOCK_ESCALATION = TABLE)
GO
