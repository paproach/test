SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserNote] (
		[NoteID]          [int] IDENTITY(1, 1) NOT NULL,
		[UserID]          [int] NOT NULL,
		[Note]            [nvarchar](500) COLLATE Hebrew_CI_AS NULL,
		[NoteDate]        [datetime] NULL,
		[CountHugs]       [int] NULL,
		[DailyReport]     [bit] NULL,
		[TypeNoteID]      [int] NULL,
		CONSTRAINT [PK_UserNote]
		PRIMARY KEY
		CLUSTERED
		([NoteID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserNote]
	ADD
	CONSTRAINT [DF_UserNote_CountHugs]
	DEFAULT ((0)) FOR [CountHugs]
GO
ALTER TABLE [dbo].[UserNote]
	ADD
	CONSTRAINT [DF_UserNote_DailyReport]
	DEFAULT ('false') FOR [DailyReport]
GO
ALTER TABLE [dbo].[UserNote]
	WITH CHECK
	ADD CONSTRAINT [FK_UserNote_TypeNote]
	FOREIGN KEY ([TypeNoteID]) REFERENCES [dbo].[TypeNote] ([TypeNoteID])
ALTER TABLE [dbo].[UserNote]
	CHECK CONSTRAINT [FK_UserNote_TypeNote]

GO
ALTER TABLE [dbo].[UserNote]
	WITH CHECK
	ADD CONSTRAINT [FK_UserNote_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserNote]
	CHECK CONSTRAINT [FK_UserNote_Users]

GO
ALTER TABLE [dbo].[UserNote] SET (LOCK_ESCALATION = TABLE)
GO
