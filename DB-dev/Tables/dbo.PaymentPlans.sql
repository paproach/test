SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentPlans] (
		[PaymentPlanID]        [int] IDENTITY(1, 1) NOT NULL,
		[PaymentID]            [int] NOT NULL,
		[TransactionID]        [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[PaymentTimestamp]     [datetime] NULL,
		[Amount]               [float] NULL,
		[IsPaid]               [bit] NULL,
		CONSTRAINT [PK_PaymentPlans]
		PRIMARY KEY
		CLUSTERED
		([PaymentPlanID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PaymentPlans] SET (LOCK_ESCALATION = TABLE)
GO
