SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ContentQuestionnaireResultValue] (
		[QuestionID]            [int] NOT NULL,
		[NumberingQuestion]     [int] NOT NULL,
		[NumberingAnswer]       [int] NOT NULL,
		[ValueAnswer]           [decimal](6, 2) NOT NULL,
		CONSTRAINT [PK_ContentQuestionnaireNumberingValues]
		PRIMARY KEY
		CLUSTERED
		([QuestionID], [NumberingQuestion], [NumberingAnswer], [ValueAnswer])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentQuestionnaireResultValue]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentQuestionnaireNumberingValues_ContentQuestionnaireNumberingValues]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[ContentSurveyQuestions] ([SurveyQuestionID])
ALTER TABLE [dbo].[ContentQuestionnaireResultValue]
	CHECK CONSTRAINT [FK_ContentQuestionnaireNumberingValues_ContentQuestionnaireNumberingValues]

GO
ALTER TABLE [dbo].[ContentQuestionnaireResultValue] SET (LOCK_ESCALATION = TABLE)
GO
