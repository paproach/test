SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserPayments] (
		[UserID]              [int] NOT NULL,
		[PaymentTermFrom]     [datetime] NULL,
		[PaymentTermTo]       [datetime] NULL,
		[PaymentDate]         [datetime] NULL,
		[UserPaymentID]       [int] IDENTITY(1, 1) NOT NULL,
		[PaymentID]           [int] NULL,
		CONSTRAINT [PK_UserPayments]
		PRIMARY KEY
		CLUSTERED
		([UserPaymentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPayments] SET (LOCK_ESCALATION = TABLE)
GO
