SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SocialMediaSites] (
		[SocialMediaSiteID]       [int] IDENTITY(1, 1) NOT NULL,
		[SocialMediaSiteName]     [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[SocialMediaIcon]         [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[SocialMediaDomain]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblSocialMediaSites]
		PRIMARY KEY
		CLUSTERED
		([SocialMediaSiteID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SocialMediaSites] SET (LOCK_ESCALATION = TABLE)
GO
