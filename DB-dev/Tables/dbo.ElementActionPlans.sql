SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementActionPlans] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[ActionPlanID]      [int] NOT NULL,
		[JoinDate]          [datetime] NULL,
		[StartDatePlan]     [datetime] NULL,
		CONSTRAINT [PK_TblElementAssociatedActionPlans]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [ActionPlanID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementActionPlans]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementActionPlans_ActionPlans]
	FOREIGN KEY ([ActionPlanID]) REFERENCES [dbo].[ActionPlans] ([ActionPlanID])
ALTER TABLE [dbo].[ElementActionPlans]
	CHECK CONSTRAINT [FK_ElementActionPlans_ActionPlans]

GO
ALTER TABLE [dbo].[ElementActionPlans]
	WITH CHECK
	ADD CONSTRAINT [FK_TblElementActionPlans_TblElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementActionPlans]
	CHECK CONSTRAINT [FK_TblElementActionPlans_TblElementTypes]

GO
ALTER TABLE [dbo].[ElementActionPlans] SET (LOCK_ESCALATION = TABLE)
GO
