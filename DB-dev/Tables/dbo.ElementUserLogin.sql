SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementUserLogin] (
		[ElementUserLoginID]     [int] IDENTITY(1, 1) NOT NULL,
		[ElemetTypeID]           [int] NOT NULL,
		[ElementID]              [int] NOT NULL,
		[UserID]                 [int] NOT NULL,
		[LastLogin]              [datetime] NULL,
		CONSTRAINT [PK_ElementUserLogin]
		PRIMARY KEY
		CLUSTERED
		([ElemetTypeID], [ElementID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementUserLogin] SET (LOCK_ESCALATION = TABLE)
GO
