SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserHappyProgress] (
		[UserID]              [int] NOT NULL,
		[dateDaily]           [date] NOT NULL,
		[HappyProgressID]     [int] NULL,
		CONSTRAINT [PK_UserHappyProgress]
		PRIMARY KEY
		CLUSTERED
		([UserID], [dateDaily])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserHappyProgress]
	ADD
	CONSTRAINT [DF_UserHappyProgress_HappyProgress]
	DEFAULT ((0)) FOR [HappyProgressID]
GO
ALTER TABLE [dbo].[UserHappyProgress]
	WITH CHECK
	ADD CONSTRAINT [FK_UserHappyProgress_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserHappyProgress]
	CHECK CONSTRAINT [FK_UserHappyProgress_Users]

GO
ALTER TABLE [dbo].[UserHappyProgress] SET (LOCK_ESCALATION = TABLE)
GO
