SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AboutTeamMembers] (
		[ID]        [int] IDENTITY(1, 1) NOT NULL,
		[Pos]       [int] NULL,
		[Name]      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Image]     [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[About]     [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_AboutTeamMembers]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AboutTeamMembers] SET (LOCK_ESCALATION = TABLE)
GO
