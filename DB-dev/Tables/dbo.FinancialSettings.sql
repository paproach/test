SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[FinancialSettings] (
		[ID]                [int] NOT NULL,
		[MonthlyCost]       [float] NULL,
		[YearlyCost]        [float] NULL,
		[VATPercentage]     [float] NULL,
		CONSTRAINT [PK_FinancialSettings]
		PRIMARY KEY
		CLUSTERED
		([ID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FinancialSettings] SET (LOCK_ESCALATION = TABLE)
GO
