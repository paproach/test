SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Idea] (
		[IdeaID]            [int] IDENTITY(1, 1) NOT NULL,
		[IdeaText]          [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[UserID]            [int] NULL,
		[SourcePage]        [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Date]              [datetime] NULL,
		[ElementTypeID]     [int] NULL,
		[ElementID]         [int] NULL,
		CONSTRAINT [PK_Idea]
		PRIMARY KEY
		CLUSTERED
		([IdeaID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Idea]
	ADD
	CONSTRAINT [DF_Idea_IdeaText]
	DEFAULT ((0)) FOR [IdeaText]
GO
ALTER TABLE [dbo].[Idea]
	WITH CHECK
	ADD CONSTRAINT [FK_Idea_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Idea]
	CHECK CONSTRAINT [FK_Idea_Users]

GO
ALTER TABLE [dbo].[Idea] SET (LOCK_ESCALATION = TABLE)
GO
