SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralPagesFAQs] (
		[FAQID]         [int] IDENTITY(1, 1) NOT NULL,
		[Question]      [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Answer]        [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[IsVisible]     [bit] NULL,
		[Pos]           [int] NULL,
		CONSTRAINT [PK_GeneralPagesFAQs]
		PRIMARY KEY
		CLUSTERED
		([FAQID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralPagesFAQs]
	ADD
	CONSTRAINT [DF_GeneralPagesFAQs_Pos]
	DEFAULT ((999)) FOR [Pos]
GO
ALTER TABLE [dbo].[GeneralPagesFAQs] SET (LOCK_ESCALATION = TABLE)
GO
