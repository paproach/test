SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserCommunities] (
		[UserID]                [int] NOT NULL,
		[CommunityID]           [int] NOT NULL,
		[IsCommunityHidden]     [bit] NULL,
		[IsUserHidden]          [bit] NULL,
		[PlanStartDate]         [datetime] NULL,
		[StatusApproval]        [int] NULL,
		[Favorite]              [int] NULL,
		[JoinStatus]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[UserAnonymous]         [bit] NULL,
		[CommunityType]         [int] NULL,
		CONSTRAINT [PK_TblUserCommunities]
		PRIMARY KEY
		CLUSTERED
		([UserID], [CommunityID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserCommunities]
	ADD
	CONSTRAINT [DF_UserCommunities_JoinStatus]
	DEFAULT ('Active') FOR [JoinStatus]
GO
ALTER TABLE [dbo].[UserCommunities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserCommunities_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserCommunities]
	CHECK CONSTRAINT [FK_TblUserCommunities_TblUsers]

GO
ALTER TABLE [dbo].[UserCommunities]
	WITH CHECK
	ADD CONSTRAINT [FK_UserCommunities_Communities]
	FOREIGN KEY ([CommunityID]) REFERENCES [dbo].[Communities] ([CommunityID])
ALTER TABLE [dbo].[UserCommunities]
	CHECK CONSTRAINT [FK_UserCommunities_Communities]

GO
ALTER TABLE [dbo].[UserCommunities]
	WITH CHECK
	ADD CONSTRAINT [FK_UserCommunities_StatusApproval]
	FOREIGN KEY ([StatusApproval]) REFERENCES [dbo].[CommunityStatusOptions] ([CommunityStatusID])
ALTER TABLE [dbo].[UserCommunities]
	CHECK CONSTRAINT [FK_UserCommunities_StatusApproval]

GO
ALTER TABLE [dbo].[UserCommunities] SET (LOCK_ESCALATION = TABLE)
GO
