SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[webpages_UsersInRoles] (
		[UserId]     [int] NOT NULL,
		[RoleId]     [int] NOT NULL,
		CONSTRAINT [PK__webpages__AF2760AD4F52B2DB]
		PRIMARY KEY
		CLUSTERED
		([UserId], [RoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]
	WITH CHECK
	ADD CONSTRAINT [fk_RoleId]
	FOREIGN KEY ([RoleId]) REFERENCES [dbo].[webpages_Roles] ([RoleId])
ALTER TABLE [dbo].[webpages_UsersInRoles]
	CHECK CONSTRAINT [fk_RoleId]

GO
ALTER TABLE [dbo].[webpages_UsersInRoles]
	WITH CHECK
	ADD CONSTRAINT [fk_UserId]
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserProfile] ([UserId])
ALTER TABLE [dbo].[webpages_UsersInRoles]
	CHECK CONSTRAINT [fk_UserId]

GO
ALTER TABLE [dbo].[webpages_UsersInRoles] SET (LOCK_ESCALATION = TABLE)
GO
