SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserSessionActivity] (
		[UserSessionActivityID]     [int] IDENTITY(1, 1) NOT NULL,
		[SessionID]                 [int] NULL,
		[UserID]                    [int] NULL,
		[SessionActivityID]         [int] NULL,
		[PageName]                  [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Date]                      [datetime] NULL,
		CONSTRAINT [PK_UserSessionActivity]
		PRIMARY KEY
		CLUSTERED
		([UserSessionActivityID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSessionActivity]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSessionActivity_Sessions]
	FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Sessions] ([SessionID])
ALTER TABLE [dbo].[UserSessionActivity]
	CHECK CONSTRAINT [FK_UserSessionActivity_Sessions]

GO
ALTER TABLE [dbo].[UserSessionActivity]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSessionActivity_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserSessionActivity]
	CHECK CONSTRAINT [FK_UserSessionActivity_Users]

GO
ALTER TABLE [dbo].[UserSessionActivity] SET (LOCK_ESCALATION = TABLE)
GO
