SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Communities] (
		[CommunityID]                             [int] IDENTITY(1, 1) NOT NULL,
		[Name]                                    [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Slogan]                                  [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Logo]                                    [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Icon]                                    [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Objective1]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[Objective2]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[Objective3]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[InvitationText]                          [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[InvitationVideo]                         [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[ShortDescription]                        [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[LongDescription]                         [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[TargetAudienceDesc]                      [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[ParentCommunityID]                       [int] NULL,
		[CommunityStatusID]                       [int] NULL,
		[Requirements]                            [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[CreationDate]                            [datetime] NULL,
		[LaunchDate]                              [datetime] NULL,
		[EndDate]                                 [datetime] NULL,
		[PaymentPolicyID]                         [int] NULL,
		[LifeCycleID]                             [int] NULL,
		[IsShownInUserCommunityList]              [bit] NULL,
		[CommunityIDDisclosurePolicyID]           [bit] NULL,
		[IsGatedCommunity]                        [bit] NULL,
		[IsSponsored]                             [bit] NULL,
		[SponsorID]                               [int] NULL,
		[Likes]                                   [int] NULL,
		[Dislikes]                                [int] NULL,
		[IsBuddyAndSoulCommunity]                 [bit] NULL,
		[LeaderTypeID]                            [int] NULL,
		[MemberIDDisclosurePolicyID]              [int] NULL,
		[ContentViewPermissionsID]                [int] NULL,
		[ContentCreationPolicyID]                 [int] NULL,
		[ForumEntriesAuthorizationPolicyID]       [int] NULL,
		[RecommendationAuthorizationPolicyID]     [int] NULL,
		[MaximumMembersCount]                     [int] NULL,
		[IsOpenedToNewMembers]                    [bit] NULL,
		[IsAcceptanceCriteriaUsed]                [bit] NULL,
		[AreSubCommunitiesAllowed]                [bit] NULL,
		[AreSubCommunitiesCharged]                [bit] NULL,
		[SubCommunityRoyaltyPolicyID]             [int] NULL,
		[SubCommunityRoyaltyValue]                [float] NULL,
		[CreatorUserID]                           [int] NULL,
		[viewCount]                               [int] NULL,
		[HugsCount]                               [int] NULL,
		[Gender]                                  [nchar](10) COLLATE Hebrew_CI_AS NULL,
		[GatedMinAge]                             [int] NULL,
		[GatedMaxAge]                             [int] NULL,
		[GatedGenderID]                           [int] NULL,
		[StatusApproval]                          [int] NULL,
		[Favorite]                                [int] NULL,
		[IsUponLeaderApproval]                    [bit] NULL,
		[GatedCommunityID]                        [int] NULL,
		CONSTRAINT [PK_TblCommunities]
		PRIMARY KEY
		CLUSTERED
		([CommunityID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Communities]
	ADD
	CONSTRAINT [DF_Communities_CreatorUserID]
	DEFAULT ((1)) FOR [CreatorUserID]
GO
ALTER TABLE [dbo].[Communities]
	ADD
	CONSTRAINT [DF_Communities_MinAge]
	DEFAULT ((0)) FOR [GatedMinAge]
GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_Communities_StatusApproval]
	FOREIGN KEY ([StatusApproval]) REFERENCES [dbo].[CommunityStatusOptions] ([CommunityStatusID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_Communities_StatusApproval]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblCommunityLifeCycleOptions]
	FOREIGN KEY ([LifeCycleID]) REFERENCES [dbo].[CommunityLifeCycleOptions] ([CommunityLifeCycleID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblCommunityLifeCycleOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblCommunityStatusOptions]
	FOREIGN KEY ([CommunityStatusID]) REFERENCES [dbo].[CommunityStatusOptions] ([CommunityStatusID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblCommunityStatusOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblContentCreationPolicyOptions]
	FOREIGN KEY ([ContentCreationPolicyID]) REFERENCES [dbo].[ContentCreationPolicyOptions] ([ContentAuthorizationPolicyID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblContentCreationPolicyOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblContentCreationPolicyOptions1]
	FOREIGN KEY ([ForumEntriesAuthorizationPolicyID]) REFERENCES [dbo].[ContentCreationPolicyOptions] ([ContentAuthorizationPolicyID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblContentCreationPolicyOptions1]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblContentViewPolicyOptions]
	FOREIGN KEY ([ContentViewPermissionsID]) REFERENCES [dbo].[ContentViewPolicyOptions] ([ContentViewPermissionsID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblContentViewPolicyOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblMemberIDDisclosurePolicyOptions]
	FOREIGN KEY ([MemberIDDisclosurePolicyID]) REFERENCES [dbo].[MemberIDDisclosurePolicyOptions] ([MemberIDDisclosurePolicyID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblMemberIDDisclosurePolicyOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblPaymentPolicyOptions]
	FOREIGN KEY ([PaymentPolicyID]) REFERENCES [dbo].[PaymentPolicyOptions] ([PaymentPolicyID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblPaymentPolicyOptions]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblSponsors]
	FOREIGN KEY ([SponsorID]) REFERENCES [dbo].[Sponsors] ([SponsorID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblSponsors]

GO
ALTER TABLE [dbo].[Communities]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunities_TblUsers]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Communities]
	CHECK CONSTRAINT [FK_TblCommunities_TblUsers]

GO
ALTER TABLE [dbo].[Communities] SET (LOCK_ESCALATION = TABLE)
GO
