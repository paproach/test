SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralPagesFreeStyle] (
		[PageID]            [int] IDENTITY(1, 1) NOT NULL,
		[Keywords]          [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Description]       [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Title]             [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[PageContent]       [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[URL]               [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[CreatorUserID]     [int] NULL,
		[PageTypeID]        [int] NULL,
		CONSTRAINT [PK_GeneralPagesFreeStyle]
		PRIMARY KEY
		CLUSTERED
		([PageID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralPagesFreeStyle] SET (LOCK_ESCALATION = TABLE)
GO
