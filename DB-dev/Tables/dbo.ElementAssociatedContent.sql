SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementAssociatedContent] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[ContentID]         [int] NOT NULL,
		CONSTRAINT [PK_TblElementAssociatedContent]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [ContentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementAssociatedContent]
	WITH CHECK
	ADD CONSTRAINT [FK_TblElementAssociatedContent_TblElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementAssociatedContent]
	CHECK CONSTRAINT [FK_TblElementAssociatedContent_TblElementTypes]

GO
ALTER TABLE [dbo].[ElementAssociatedContent] SET (LOCK_ESCALATION = TABLE)
GO
