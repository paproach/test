SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Icons] (
		[IconID]            [int] IDENTITY(1, 1) NOT NULL,
		[IconUrl]           [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[ElementTypeID]     [int] NULL,
		CONSTRAINT [PK_Icons]
		PRIMARY KEY
		CLUSTERED
		([IconID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Icons] SET (LOCK_ESCALATION = TABLE)
GO
