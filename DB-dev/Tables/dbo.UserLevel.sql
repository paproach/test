SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserLevel] (
		[UserLevelID]     [int] IDENTITY(1, 1) NOT NULL,
		[UserID]          [int] NULL,
		[LevelID]         [int] NULL,
		[Date]            [datetime] NULL,
		CONSTRAINT [PK_UserLevel]
		PRIMARY KEY
		CLUSTERED
		([UserLevelID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserLevel]
	WITH CHECK
	ADD CONSTRAINT [FK_UserLevel_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserLevel]
	CHECK CONSTRAINT [FK_UserLevel_Users]

GO
ALTER TABLE [dbo].[UserLevel] SET (LOCK_ESCALATION = TABLE)
GO
