SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AspNetUserRoles] (
		[UserId]     [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[RoleId]     [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_dbo.AspNetUserRoles]
		PRIMARY KEY
		CLUSTERED
		([UserId], [RoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserRoles]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
	FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles]
	CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]

GO
ALTER TABLE [dbo].[AspNetUserRoles]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles]
	CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]

GO
ALTER TABLE [dbo].[AspNetUserRoles] SET (LOCK_ESCALATION = TABLE)
GO
