SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditData] (
		[ActionID]                     [int] IDENTITY(1, 1) NOT NULL,
		[ActionTypeID]                 [int] NULL,
		[Timestamp]                    [datetime] NULL,
		[PerformedByElementTypeID]     [int] NULL,
		[CommunityID]                  [int] NULL,
		[SpecialForceID]               [int] NULL,
		[UserID]                       [int] NULL,
		[ContentTypeID]                [int] NULL,
		[ContentID]                    [int] NULL,
		[SubContentID]                 [int] NULL,
		[BonusPoints]                  [float] NULL,
		[Description]                  [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[CompletedID]                  [int] NULL,
		CONSTRAINT [PK_TblAuditData]
		PRIMARY KEY
		CLUSTERED
		([ActionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'1-done, 2-half', 'SCHEMA', N'dbo', 'TABLE', N'AuditData', 'COLUMN', N'CompletedID'
GO
ALTER TABLE [dbo].[AuditData] SET (LOCK_ESCALATION = TABLE)
GO
