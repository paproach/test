SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Triggers] (
		[TriggerID]             [int] IDENTITY(1, 1) NOT NULL,
		[TriggerName]           [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[TriggerSQL]            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[IsEnabled]             [bit] NULL,
		[Command]               [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[LastAuditActionID]     [int] NULL,
		[LastExec]              [datetime] NULL,
		[IntervalInMinutes]     [int] NULL,
		[StartFirstRun]         [datetime] NULL,
		CONSTRAINT [PK_TblTriggers]
		PRIMARY KEY
		CLUSTERED
		([TriggerID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Triggers] SET (LOCK_ESCALATION = TABLE)
GO
