SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentQueResultFeedback] (
		[QueResultFeedbackID]     [int] IDENTITY(1, 1) NOT NULL,
		[QuestionID]              [int] NULL,
		[ContentID]               [int] NULL,
		[ResultType]              [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[MinNumberAnswer]         [decimal](6, 2) NULL,
		[MaxNumberAnswer]         [decimal](6, 2) NULL,
		[MinNumberQuestion1]      [int] NULL,
		[MaxNumberQuestion1]      [int] NULL,
		[MinNumberQuestion2]      [int] NULL,
		[MaxNumberQuestion2]      [int] NULL,
		[TextFeadback]            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[MathCalc]                [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[NumberGroup]             [int] NULL,
		[NumberAnswer]            [int] NULL,
		[MainTagID]               [int] NULL,
		[EdgeTagID]               [int] NULL,
		[IsShowUserScore]         [bit] NULL,
		CONSTRAINT [PK_QueResultFeedback_1]
		PRIMARY KEY
		CLUSTERED
		([QueResultFeedbackID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentQueResultFeedback]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentQueResultFeedback_ContentSurveyQuestions]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[ContentSurveyQuestions] ([SurveyQuestionID])
ALTER TABLE [dbo].[ContentQueResultFeedback]
	CHECK CONSTRAINT [FK_ContentQueResultFeedback_ContentSurveyQuestions]

GO
ALTER TABLE [dbo].[ContentQueResultFeedback] SET (LOCK_ESCALATION = TABLE)
GO
