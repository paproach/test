SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webpages_Membership] (
		[UserId]                                      [int] NOT NULL,
		[CreateDate]                                  [datetime] NULL,
		[ConfirmationToken]                           [nvarchar](128) COLLATE Hebrew_CI_AS NULL,
		[IsConfirmed]                                 [bit] NULL,
		[LastPasswordFailureDate]                     [datetime] NULL,
		[PasswordFailuresSinceLastSuccess]            [int] NOT NULL,
		[Password]                                    [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[PasswordChangedDate]                         [datetime] NULL,
		[PasswordSalt]                                [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[PasswordVerificationToken]                   [nvarchar](128) COLLATE Hebrew_CI_AS NULL,
		[PasswordVerificationTokenExpirationDate]     [datetime] NULL,
		CONSTRAINT [PK__webpages__1788CC4C42ECDBF6]
		PRIMARY KEY
		CLUSTERED
		([UserId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_Membership]
	ADD
	CONSTRAINT [DF__webpages___IsCon__44D52468]
	DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership]
	ADD
	CONSTRAINT [DF__webpages___Passw__45C948A1]
	DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[webpages_Membership] SET (LOCK_ESCALATION = TABLE)
GO
