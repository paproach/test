SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentViewPolicyOptions] (
		[ContentViewPermissionsID]     [int] NOT NULL,
		[ContentViewPermissions]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblContentViewPermissions]
		PRIMARY KEY
		CLUSTERED
		([ContentViewPermissionsID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentViewPolicyOptions] SET (LOCK_ESCALATION = TABLE)
GO
