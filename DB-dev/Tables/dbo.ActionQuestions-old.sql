SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionQuestions-old] (
		[ActionID]       [int] NOT NULL,
		[QuestionID]     [int] NOT NULL,
		[Question]       [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_ActionQuestion_1]
		PRIMARY KEY
		CLUSTERED
		([ActionID], [QuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQuestions-old]
	WITH CHECK
	ADD CONSTRAINT [FK_ActionQuestion_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionQuestions-old]
	CHECK CONSTRAINT [FK_ActionQuestion_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionQuestions-old] SET (LOCK_ESCALATION = TABLE)
GO
