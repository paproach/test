SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonalityTraitsQuestionnaireScore] (
		[QuestionID]        [int] NOT NULL,
		[AnswerID]          [int] NOT NULL,
		[Answer]            [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[ScoringAnswer]     [int] NULL,
		CONSTRAINT [PK_PersonalityTraitsQuestionnaireScore]
		PRIMARY KEY
		CLUSTERED
		([QuestionID], [AnswerID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PersonalityTraitsQuestionnaireScore]
	WITH CHECK
	ADD CONSTRAINT [FK_PersonalityTraitsQuestionnaireScore_PersonalityTraitsQuestionnaire]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[PersonalityTraitsQuestionnaire] ([QuestionID])
ALTER TABLE [dbo].[PersonalityTraitsQuestionnaireScore]
	CHECK CONSTRAINT [FK_PersonalityTraitsQuestionnaireScore_PersonalityTraitsQuestionnaire]

GO
ALTER TABLE [dbo].[PersonalityTraitsQuestionnaireScore] SET (LOCK_ESCALATION = TABLE)
GO
