SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecurrencyOptions] (
		[RecurrencyUnitID]       [int] NOT NULL,
		[RecurrencyUnitDesc]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_RecurrencyOptions]
		PRIMARY KEY
		CLUSTERED
		([RecurrencyUnitID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RecurrencyOptions] SET (LOCK_ESCALATION = TABLE)
GO
