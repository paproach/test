SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApprovalStatus] (
		[StatusID]       [int] NOT NULL,
		[StatusDesc]     [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_ApprovalStatus]
		PRIMARY KEY
		CLUSTERED
		([StatusID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalStatus] SET (LOCK_ESCALATION = TABLE)
GO
