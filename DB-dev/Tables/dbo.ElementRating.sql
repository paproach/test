SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementRating] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[UserID]            [int] NOT NULL,
		[Rating]            [int] NULL,
		[LastDate]          [datetime] NULL,
		CONSTRAINT [PK_TblElementRating]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementRating]
	WITH CHECK
	ADD CONSTRAINT [FK_TblElementRating_TblElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementRating]
	CHECK CONSTRAINT [FK_TblElementRating_TblElementTypes]

GO
ALTER TABLE [dbo].[ElementRating]
	WITH CHECK
	ADD CONSTRAINT [FK_TblElementRating_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ElementRating]
	CHECK CONSTRAINT [FK_TblElementRating_TblUsers]

GO
ALTER TABLE [dbo].[ElementRating] SET (LOCK_ESCALATION = TABLE)
GO
