SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElementLinks] (
		[ElementTypeID]         [int] NOT NULL,
		[ElementID]             [int] NOT NULL,
		[LinkID]                [int] IDENTITY(1, 1) NOT NULL,
		[SocialMediaSiteID]     [int] NOT NULL,
		[Link]                  [nvarchar](255) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_TblElementLinks]
		PRIMARY KEY
		CLUSTERED
		([Link])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementLinks]
	WITH CHECK
	ADD CONSTRAINT [FK_TblElementLinks_TblSocialMediaSites]
	FOREIGN KEY ([SocialMediaSiteID]) REFERENCES [dbo].[SocialMediaSites] ([SocialMediaSiteID])
ALTER TABLE [dbo].[ElementLinks]
	CHECK CONSTRAINT [FK_TblElementLinks_TblSocialMediaSites]

GO
ALTER TABLE [dbo].[ElementLinks] SET (LOCK_ESCALATION = TABLE)
GO
