SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatSystemQuestions] (
		[CoachatSystemQuestionID]     [int] IDENTITY(1, 1) NOT NULL,
		[CoachatSystemQuestion]       [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_CoachatSystemQuestions]
		PRIMARY KEY
		CLUSTERED
		([CoachatSystemQuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatSystemQuestions] SET (LOCK_ESCALATION = TABLE)
GO
