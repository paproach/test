SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentSurveyQuestions] (
		[SurveyQuestionID]     [int] IDENTITY(1, 1) NOT NULL,
		[ContentID]            [int] NULL,
		[UserID]               [int] NULL,
		[Question]             [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Date]                 [datetime] NULL,
		[ViewCount]            [int] NULL,
		[HugsCount]            [int] NULL,
		[StatusApproval]       [int] NULL,
		CONSTRAINT [PK_TblContentSurveyQuestions]
		PRIMARY KEY
		CLUSTERED
		([SurveyQuestionID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentSurveyQuestions]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentSurveyQuestions_TblContent]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ContentSurveyQuestions]
	CHECK CONSTRAINT [FK_TblContentSurveyQuestions_TblContent]

GO
ALTER TABLE [dbo].[ContentSurveyQuestions] SET (LOCK_ESCALATION = TABLE)
GO
