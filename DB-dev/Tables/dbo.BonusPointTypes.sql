SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusPointTypes] (
		[BonusTypeID]          [int] IDENTITY(1, 1) NOT NULL,
		[BonusName]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[BonusDescription]     [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[AuditActionType]      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[BonusPoints]          [float] NULL,
		[BonusIcon]            [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblBonusPointTypes]
		PRIMARY KEY
		CLUSTERED
		([BonusTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BonusPointTypes] SET (LOCK_ESCALATION = TABLE)
GO
