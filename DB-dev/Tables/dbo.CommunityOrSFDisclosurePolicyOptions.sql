SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommunityOrSFDisclosurePolicyOptions] (
		[CommunityIDDisclosurePolicyID]     [int] NOT NULL,
		[CommunityIDDisclosurePolicy]       [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblCommunityIDDisclosurePolicyID]
		PRIMARY KEY
		CLUSTERED
		([CommunityIDDisclosurePolicyID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunityOrSFDisclosurePolicyOptions] SET (LOCK_ESCALATION = TABLE)
GO
