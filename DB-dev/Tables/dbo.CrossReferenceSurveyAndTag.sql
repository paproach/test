SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CrossReferenceSurveyAndTag] (
		[CrossReferenceID]     [int] IDENTITY(1, 1) NOT NULL,
		[ElemenyTypeID]        [int] NULL,
		[ContentID]            [int] NULL,
		[ActionID]             [int] NULL,
		[QuestionID]           [int] NULL,
		[Numbering]            [int] NULL,
		[AnswerID]             [int] NULL,
		[TagID]                [int] NULL,
		CONSTRAINT [PK_CrossReferenceSurveyAndTag]
		PRIMARY KEY
		CLUSTERED
		([CrossReferenceID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndTag_Content]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndTag_Content]

GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndTag_ContentSurveyQuestions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndTag_ContentSurveyQuestions]

GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndTag_Tags]
	FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tags] ([TagID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndTag_Tags]

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CrossReferenceSurveyAndTag]
	ON [dbo].[CrossReferenceSurveyAndTag] ([ContentID], [AnswerID], [QuestionID], [TagID], [ActionID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndTag] SET (LOCK_ESCALATION = TABLE)
GO
