SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeActionPlan] (
		[TypeActionPlanID]     [int] IDENTITY(1, 1) NOT NULL,
		[TypeActionName]       [nvarchar](250) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_TypeActionPlan]
		PRIMARY KEY
		CLUSTERED
		([TypeActionPlanID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TypeActionPlan] SET (LOCK_ESCALATION = TABLE)
GO
