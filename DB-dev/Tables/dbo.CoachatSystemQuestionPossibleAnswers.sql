SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatSystemQuestionPossibleAnswers] (
		[CoachatSystemQuestionID]           [int] NULL,
		[CoachatSystemQuestionAnswerID]     [int] IDENTITY(1, 1) NOT NULL,
		[CoachatSystemQuestionAnswer]       [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_CoachatSystemQuestionPossibleAnswers]
		PRIMARY KEY
		CLUSTERED
		([CoachatSystemQuestionAnswerID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatSystemQuestionPossibleAnswers] SET (LOCK_ESCALATION = TABLE)
GO
