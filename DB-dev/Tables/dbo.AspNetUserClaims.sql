SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AspNetUserClaims] (
		[Id]             [int] IDENTITY(1, 1) NOT NULL,
		[UserId]         [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[ClaimType]      [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ClaimValue]     [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_dbo.AspNetUserClaims]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserClaims]
	CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]

GO
ALTER TABLE [dbo].[AspNetUserClaims] SET (LOCK_ESCALATION = TABLE)
GO
