SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementContents] (
		[ElementTypeID]       [int] NOT NULL,
		[ElementID]           [int] NOT NULL,
		[ContentID]           [int] NOT NULL,
		[TimeID]              [int] NULL,
		[IsContentSplit]      [bit] NULL,
		[ContentIDLinked]     [int] NULL,
		[TagIDLinked]         [int] NULL,
		CONSTRAINT [PK_ElementContents]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [ContentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementContents]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementContents_Content]
	FOREIGN KEY ([ContentIDLinked]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ElementContents]
	CHECK CONSTRAINT [FK_ElementContents_Content]

GO
ALTER TABLE [dbo].[ElementContents]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementContents_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementContents]
	CHECK CONSTRAINT [FK_ElementContents_ElementTypes]

GO
ALTER TABLE [dbo].[ElementContents]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementContents_Tags]
	FOREIGN KEY ([TagIDLinked]) REFERENCES [dbo].[Tags] ([TagID])
ALTER TABLE [dbo].[ElementContents]
	CHECK CONSTRAINT [FK_ElementContents_Tags]

GO
ALTER TABLE [dbo].[ElementContents] SET (LOCK_ESCALATION = TABLE)
GO
