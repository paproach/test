SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementShare] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[UserID]            [int] NOT NULL,
		[NetworkTypeID]     [int] NOT NULL,
		[Date]              [datetime] NULL,
		CONSTRAINT [PK_ElementShare]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [UserID], [NetworkTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementShare]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementShare_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementShare]
	CHECK CONSTRAINT [FK_ElementShare_ElementTypes]

GO
ALTER TABLE [dbo].[ElementShare]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementShare_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ElementShare]
	CHECK CONSTRAINT [FK_ElementShare_Users]

GO
EXEC sp_addextendedproperty N'MS_Description', N'1-facebook, 2-twitter, 3-linkedIn, 4-googlePlus', 'SCHEMA', N'dbo', 'TABLE', N'ElementShare', 'COLUMN', N'NetworkTypeID'
GO
ALTER TABLE [dbo].[ElementShare] SET (LOCK_ESCALATION = TABLE)
GO
