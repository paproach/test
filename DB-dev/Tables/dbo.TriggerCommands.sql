SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TriggerCommands] (
		[TriggerCommand]           [nvarchar](50) COLLATE Hebrew_CI_AS NOT NULL,
		[TriggerCommandUIName]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TriggerCommands]
		PRIMARY KEY
		CLUSTERED
		([TriggerCommand])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TriggerCommands] SET (LOCK_ESCALATION = TABLE)
GO
