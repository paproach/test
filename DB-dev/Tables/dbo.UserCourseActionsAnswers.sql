SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserCourseActionsAnswers] (
		[UserID]             [int] NOT NULL,
		[ActionID]           [int] NOT NULL,
		[ReportActionID]     [int] NOT NULL,
		[DataIndex]          [int] NOT NULL,
		[Data]               [nvarchar](max) COLLATE Hebrew_CI_AS NOT NULL,
		[Timestamp]          [datetime] NOT NULL,
		[TypeSupplement]     [int] NULL,
		[FileSupplement]     [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_UserSpecialForceActionsData]
		PRIMARY KEY
		CLUSTERED
		([UserID], [ActionID], [ReportActionID], [DataIndex])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserCourseActionsAnswers] SET (LOCK_ESCALATION = TABLE)
GO
