SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentDebateArgs] (
		[DebateArgID]        [int] IDENTITY(1, 1) NOT NULL,
		[ContentID]          [int] NOT NULL,
		[Side]               [int] NULL,
		[UserID]             [int] NULL,
		[Argument]           [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[AgreeCount]         [int] NULL,
		[Date]               [datetime] NULL,
		[HugsCount]          [int] NULL,
		[ViewCount]          [int] NULL,
		[StatusApproval]     [int] NULL,
		[UserAnonymous]      [bit] NULL,
		CONSTRAINT [PK_TblContentDebateArgs]
		PRIMARY KEY
		CLUSTERED
		([DebateArgID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentDebateArgs]
	ADD
	CONSTRAINT [DF_ContentDebateArgs_AgreeCount]
	DEFAULT ((0)) FOR [AgreeCount]
GO
ALTER TABLE [dbo].[ContentDebateArgs]
	ADD
	CONSTRAINT [DF_ContentDebateArgs_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[ContentDebateArgs]
	ADD
	CONSTRAINT [DF_ContentDebateArgs_UserID]
	DEFAULT ((1)) FOR [UserID]
GO
ALTER TABLE [dbo].[ContentDebateArgs]
	ADD
	CONSTRAINT [DF_ContentDebateArgs_ViewCount]
	DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[ContentDebateArgs]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentDebateArgs_TblContent]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ContentDebateArgs]
	CHECK CONSTRAINT [FK_TblContentDebateArgs_TblContent]

GO
ALTER TABLE [dbo].[ContentDebateArgs]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentDebateArgs_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ContentDebateArgs]
	CHECK CONSTRAINT [FK_TblContentDebateArgs_TblUsers]

GO
ALTER TABLE [dbo].[ContentDebateArgs] SET (LOCK_ESCALATION = TABLE)
GO
