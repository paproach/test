SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentTypes] (
		[ContentTypeID]       [int] NOT NULL,
		[ContentTypeDesc]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblContentTypes]
		PRIMARY KEY
		CLUSTERED
		([ContentTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentTypes] SET (LOCK_ESCALATION = TABLE)
GO
