SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users] (
		[ID]                            [nvarchar](128) COLLATE Hebrew_CI_AS NULL,
		[UserID]                        [int] IDENTITY(1, 1) NOT NULL,
		[FirstName]                     [nvarchar](40) COLLATE Hebrew_CI_AS NULL,
		[LastName]                      [nvarchar](40) COLLATE Hebrew_CI_AS NULL,
		[Email]                         [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Password]                      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[PasswordHash]                  [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Company]                       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[JobTitle]                      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Address1]                      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Address2]                      [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[City]                          [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[State]                         [nvarchar](10) COLLATE Hebrew_CI_AS NULL,
		[Country]                       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[ZipCode]                       [varchar](5) COLLATE Hebrew_CI_AS NULL,
		[Phone]                         [nvarchar](20) COLLATE Hebrew_CI_AS NULL,
		[Mobile]                        [nvarchar](20) COLLATE Hebrew_CI_AS NULL,
		[Fax]                           [nvarchar](20) COLLATE Hebrew_CI_AS NULL,
		[IsPromotionalMailsAllowed]     [bit] NULL,
		[Website]                       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[IP]                            [char](15) COLLATE Hebrew_CI_AS NULL,
		[RegistrationDate]              [datetime] NULL,
		[UserGuid]                      [varchar](50) COLLATE Hebrew_CI_AS NULL,
		[UserName]                      [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[APIKey]                        [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[ImageURL]                      [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[UserStatus]                    [int] NULL,
		[LastLogInDate]                 [datetime] NULL,
		[BirthDate]                     [datetime] NULL,
		[Gender]                        [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Slogan]                        [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Moto]                          [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[ViewCount]                     [int] NULL,
		[HugsCount]                     [int] NULL,
		[SumTraitExtraversion]          [float] NULL,
		[SumTraitAgreeableness]         [float] NULL,
		[SumTraitConscientiousness]     [float] NULL,
		[SumTraitNeuroticism]           [float] NULL,
		[SumTraitOpenness]              [float] NULL,
		[ApprovalMail]                  [bit] NULL,
		[TimeZone]                      [int] NULL,
		[NotificationOnSite]            [bit] NULL,
		[NotificationOnEmail]           [bit] NULL,
		[PrivacyHideAlways]             [bit] NULL,
		[PrivacyPublishAnonymously]     [bit] NULL,
		[Languages]                     [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[IsPaid]                        [int] NULL,
		CONSTRAINT [PK_TblUsers]
		PRIMARY KEY
		CLUSTERED
		([UserID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_BirthDate]
	DEFAULT ('1900-1-1') FOR [BirthDate]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_ImageURL]
	DEFAULT (N'/Content/Images/User/User5.png') FOR [ImageURL]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_LastLogInDate]
	DEFAULT ('1900-1-1') FOR [LastLogInDate]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_RegistrationDate]
	DEFAULT (((1)-(1))-(1900)) FOR [RegistrationDate]
GO
ALTER TABLE [dbo].[Users]
	ADD
	CONSTRAINT [DF_Users_UserStatus]
	DEFAULT ((1)) FOR [UserStatus]
GO
ALTER TABLE [dbo].[Users] SET (LOCK_ESCALATION = TABLE)
GO
