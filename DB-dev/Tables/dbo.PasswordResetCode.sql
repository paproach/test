SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PasswordResetCode] (
		[CodeID]            [int] IDENTITY(1, 1) NOT NULL,
		[CodeSendEmail]     [nvarchar](20) COLLATE Hebrew_CI_AS NULL,
		[Code]              [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_PasswordResetCode]
		PRIMARY KEY
		CLUSTERED
		([CodeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PasswordResetCode] SET (LOCK_ESCALATION = TABLE)
GO
