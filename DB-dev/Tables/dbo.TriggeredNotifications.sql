SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TriggeredNotifications] (
		[TriggeredNotificationID]            [int] IDENTITY(1, 1) NOT NULL,
		[Name]                               [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Title]                              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Notification]                       [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[UsesUserRec]                        [bit] NULL,
		[UsesCommunityRec]                   [bit] NULL,
		[UsesCourseRec]                      [bit] NULL,
		[UsesContentRec]                     [bit] NULL,
		[TargetsRelatedUser]                 [bit] NULL,
		[TargetsAllUsers]                    [bit] NULL,
		[TargetsRelatedCommunityLeader]      [bit] NULL,
		[TargetsRelatedCommunityMembers]     [bit] NULL,
		[TargetsAllCommunityLeaders]         [bit] NULL,
		[TargetsRelatedCourseCaptain]        [bit] NULL,
		[TargetsRelatedCourseMembers]        [bit] NULL,
		[TargetsAllCourseCaptains]           [bit] NULL,
		[TargetAdmin]                        [bit] NULL,
		[TriggerID]                          [int] NOT NULL,
		[TimesToReactPerTrigger]             [int] NULL,
		[TimeBetweenRecurrentReactions]      [int] NULL,
		[IsEnabled]                          [bit] NULL,
		CONSTRAINT [PK_TriggeredNotifications]
		PRIMARY KEY
		CLUSTERED
		([TriggeredNotificationID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TriggeredNotifications]
	WITH CHECK
	ADD CONSTRAINT [FK_TriggeredNotifications_Triggers]
	FOREIGN KEY ([TriggerID]) REFERENCES [dbo].[Triggers] ([TriggerID])
ALTER TABLE [dbo].[TriggeredNotifications]
	CHECK CONSTRAINT [FK_TriggeredNotifications_Triggers]

GO
ALTER TABLE [dbo].[TriggeredNotifications] SET (LOCK_ESCALATION = TABLE)
GO
