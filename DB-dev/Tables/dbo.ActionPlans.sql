SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionPlans] (
		[ActionPlanID]          [int] IDENTITY(1, 1) NOT NULL,
		[OwningCommunityID]     [int] NULL,
		[OwningCourseID]        [int] NULL,
		[Name]                  [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Image]                 [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[CreatorUserID]         [int] NULL,
		[CreationDate]          [datetime] NULL,
		[LastUpdateDate]        [datetime] NULL,
		[PublishingDate]        [datetime] NULL,
		[StartsOnDate]          [datetime] NULL,
		[ViewCount]             [int] NULL,
		[HugsCount]             [int] NULL,
		[Title]                 [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Body]                  [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Slogan]                [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[OwningUserID]          [int] NULL,
		[Icon]                  [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[TypeActionPlanID]      [int] NULL,
		CONSTRAINT [PK_TblActionPlans]
		PRIMARY KEY
		CLUSTERED
		([ActionPlanID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_CreationDate]
	DEFAULT (((1)/(1))/(1)) FOR [CreationDate]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_CreatorUserID]
	DEFAULT ((0)) FOR [CreatorUserID]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_LastUpdateDate]
	DEFAULT (((1)/(1))/(1)) FOR [LastUpdateDate]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_OwningCommunityID]
	DEFAULT ((0)) FOR [OwningCommunityID]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_OwningSpecialForceID]
	DEFAULT ((0)) FOR [OwningCourseID]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_PublishingDate]
	DEFAULT (((1)/(1))/(1)) FOR [PublishingDate]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_StartsOnDate]
	DEFAULT (((1)/(1))/(1)) FOR [StartsOnDate]
GO
ALTER TABLE [dbo].[ActionPlans]
	ADD
	CONSTRAINT [DF_ActionPlans_ViewCount]
	DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[ActionPlans]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionPlans_ActionPlans]
	FOREIGN KEY ([TypeActionPlanID]) REFERENCES [dbo].[TypeActionPlan] ([TypeActionPlanID])
ALTER TABLE [dbo].[ActionPlans]
	CHECK CONSTRAINT [FK_ActionPlans_ActionPlans]

GO
ALTER TABLE [dbo].[ActionPlans]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionPlans_SpecialForces]
	FOREIGN KEY ([OwningCourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[ActionPlans]
	CHECK CONSTRAINT [FK_ActionPlans_SpecialForces]

GO
ALTER TABLE [dbo].[ActionPlans]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionPlans_Users]
	FOREIGN KEY ([OwningUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ActionPlans]
	CHECK CONSTRAINT [FK_ActionPlans_Users]

GO
ALTER TABLE [dbo].[ActionPlans]
	WITH NOCHECK
	ADD CONSTRAINT [FK_TblActionPlans_TblCommunities]
	FOREIGN KEY ([OwningCommunityID]) REFERENCES [dbo].[Communities] ([CommunityID])
ALTER TABLE [dbo].[ActionPlans]
	CHECK CONSTRAINT [FK_TblActionPlans_TblCommunities]

GO
ALTER TABLE [dbo].[ActionPlans]
	WITH NOCHECK
	ADD CONSTRAINT [FK_TblActionPlans_TblUsers]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ActionPlans]
	CHECK CONSTRAINT [FK_TblActionPlans_TblUsers]

GO
ALTER TABLE [dbo].[ActionPlans] SET (LOCK_ESCALATION = TABLE)
GO
