SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Payments] (
		[PaymentID]              [int] IDENTITY(1, 1) NOT NULL,
		[PaymentForm]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[PaymentTimestamp]       [datetime] NULL,
		[TotalAmount]            [float] NULL,
		[MonthlyAmount]          [float] NULL,
		[IsRecurringPayment]     [bit] NULL,
		[PaymentToken]           [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[PayingUserID]           [int] NULL,
		[UsedCouponID]           [int] NULL,
		CONSTRAINT [PK_PaymentTransactions]
		PRIMARY KEY
		CLUSTERED
		([PaymentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Payments] SET (LOCK_ESCALATION = TABLE)
GO
