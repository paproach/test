SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feedback] (
		[FeedbackID]         [int] IDENTITY(1, 1) NOT NULL,
		[PlatformUseful]     [int] NULL,
		[PlatformEasy]       [int] NULL,
		[FeedbackMsg]        [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ViewName]           [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[UserID]             [int] NULL,
		CONSTRAINT [PK_Feedback]
		PRIMARY KEY
		CLUSTERED
		([FeedbackID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Feedback] SET (LOCK_ESCALATION = TABLE)
GO
