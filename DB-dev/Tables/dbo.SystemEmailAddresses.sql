SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemEmailAddresses] (
		[SystemEmailAddresseID]     [int] IDENTITY(1, 1) NOT NULL,
		[EmailAddresses]            [nchar](250) COLLATE Hebrew_CI_AS NULL,
		[EmailAddresseName]         [nchar](250) COLLATE Hebrew_CI_AS NULL,
		[EmailPasswordAddresse]     [nchar](250) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_SystemEmailAddresses]
		PRIMARY KEY
		CLUSTERED
		([SystemEmailAddresseID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemEmailAddresses] SET (LOCK_ESCALATION = TABLE)
GO
