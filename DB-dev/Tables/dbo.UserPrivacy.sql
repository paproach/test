SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserPrivacy] (
		[UserID]          [int] NOT NULL,
		[HideAlways]      [bit] NOT NULL,
		[HideContent]     [bit] NOT NULL,
		CONSTRAINT [PK_UserPrivacy]
		PRIMARY KEY
		CLUSTERED
		([UserID], [HideAlways], [HideContent])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPrivacy]
	WITH CHECK
	ADD CONSTRAINT [FK_UserPrivacy_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserPrivacy]
	CHECK CONSTRAINT [FK_UserPrivacy_Users]

GO
ALTER TABLE [dbo].[UserPrivacy] SET (LOCK_ESCALATION = TABLE)
GO
