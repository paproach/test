SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TagCategoryAssociation] (
		[TagID]             [int] NOT NULL,
		[TagCategoryID]     [int] NOT NULL,
		CONSTRAINT [PK_TblTagCatgoryAssociation]
		PRIMARY KEY
		CLUSTERED
		([TagID], [TagCategoryID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TagCategoryAssociation]
	WITH CHECK
	ADD CONSTRAINT [FK_TblTagCategoryAssociation_TblTagCategories]
	FOREIGN KEY ([TagCategoryID]) REFERENCES [dbo].[TagCategories] ([TagCategoryID])
ALTER TABLE [dbo].[TagCategoryAssociation]
	CHECK CONSTRAINT [FK_TblTagCategoryAssociation_TblTagCategories]

GO
ALTER TABLE [dbo].[TagCategoryAssociation]
	WITH CHECK
	ADD CONSTRAINT [FK_TblTagCategoryAssociation_TblTags]
	FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tags] ([TagID])
ALTER TABLE [dbo].[TagCategoryAssociation]
	CHECK CONSTRAINT [FK_TblTagCategoryAssociation_TblTags]

GO
ALTER TABLE [dbo].[TagCategoryAssociation] SET (LOCK_ESCALATION = TABLE)
GO
