SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionQueResultFeedback] (
		[QueResultFeedbackID]     [int] IDENTITY(1, 1) NOT NULL,
		[QuestionID]              [int] NULL,
		[ActionID]                [int] NULL,
		[ResultType]              [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[MinNumberAnswer]         [decimal](6, 2) NULL,
		[MaxNumberAnswer]         [decimal](6, 2) NULL,
		[MaxNumberQuestion1]      [int] NULL,
		[MinNumberQuestion2]      [int] NULL,
		[MaxNumberQuestion2]      [int] NULL,
		[MinNumberQuestion1]      [int] NULL,
		[TextFeadback]            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[MathCalc]                [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[NumberGroup]             [int] NULL,
		[NumberAnswer]            [int] NULL,
		[MainTagID]               [int] NULL,
		[EdgeTagID]               [int] NULL,
		[IsShowUserScore]         [int] NULL,
		CONSTRAINT [PK_ActionQueResultFeedback]
		PRIMARY KEY
		CLUSTERED
		([QueResultFeedbackID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQueResultFeedback]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionQueResultFeedback_SpecialForceActionQuestions]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[ActionQuestions] ([QuestionID])
ALTER TABLE [dbo].[ActionQueResultFeedback]
	CHECK CONSTRAINT [FK_ActionQueResultFeedback_SpecialForceActionQuestions]

GO
ALTER TABLE [dbo].[ActionQueResultFeedback]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionQueResultFeedback_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionQueResultFeedback]
	CHECK CONSTRAINT [FK_ActionQueResultFeedback_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionQueResultFeedback] SET (LOCK_ESCALATION = TABLE)
GO
