SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tips] (
		[TipID]              [int] IDENTITY(1, 1) NOT NULL,
		[ElementTypeID]      [int] NULL,
		[ElementID]          [int] NULL,
		[Title]              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Tip]                [nvarchar](3000) COLLATE Hebrew_CI_AS NULL,
		[Date]               [datetime] NULL,
		[UserID]             [int] NULL,
		[viewCount]          [int] NULL,
		[HugsCount]          [int] NULL,
		[StatusApproval]     [int] NULL,
		[UserAnonymous]      [bit] NULL,
		CONSTRAINT [PK_Tips]
		PRIMARY KEY
		CLUSTERED
		([TipID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tips]
	WITH CHECK
	ADD CONSTRAINT [FK_Tips_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Tips]
	CHECK CONSTRAINT [FK_Tips_Users]

GO
ALTER TABLE [dbo].[Tips] SET (LOCK_ESCALATION = TABLE)
GO
