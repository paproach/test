SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TriggeredEmails] (
		[TriggeredEmailID]                   [int] IDENTITY(1, 1) NOT NULL,
		[Name]                               [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Title]                              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[UsesUserRec]                        [bit] NULL,
		[UsesCommunityRec]                   [bit] NULL,
		[UsesCourseRec]                      [bit] NULL,
		[UsesContentRec]                     [bit] NULL,
		[TargetsRelatedUser]                 [bit] NULL,
		[TargetsAllUsers]                    [bit] NULL,
		[TargetsRelatedCommunityLeader]      [bit] NULL,
		[TargetsRelatedCommunityMembers]     [bit] NULL,
		[TargetsAllCommunityLeaders]         [bit] NULL,
		[TargetsRelatedCourseCaptain]        [bit] NULL,
		[TargetsRelatedCourseMembers]        [bit] NULL,
		[TargetsAllCourseCaptains]           [bit] NULL,
		[TargetAdmin]                        [bit] NULL,
		[TimesToReactPerTrigger]             [int] NULL,
		[TimeBetweenRecurrentReactions]      [int] NULL,
		[IsEnabled]                          [bit] NULL,
		[EmailBody]                          [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[TriggerID]                          [int] NOT NULL,
		[SystemEmailAddresseID]              [int] NULL,
		CONSTRAINT [PK_TriggeredEmails]
		PRIMARY KEY
		CLUSTERED
		([TriggeredEmailID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TriggeredEmails]
	WITH CHECK
	ADD CONSTRAINT [FK_TriggeredEmails_TriggeredEmails]
	FOREIGN KEY ([TriggerID]) REFERENCES [dbo].[Triggers] ([TriggerID])
ALTER TABLE [dbo].[TriggeredEmails]
	CHECK CONSTRAINT [FK_TriggeredEmails_TriggeredEmails]

GO
ALTER TABLE [dbo].[TriggeredEmails] SET (LOCK_ESCALATION = TABLE)
GO
