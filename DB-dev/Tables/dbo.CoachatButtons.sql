SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatButtons] (
		[CoachatButtonID]        [int] IDENTITY(1, 1) NOT NULL,
		[CoachatButtonDesc]      [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[CoachatButtonClass]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_CoachatButtons]
		PRIMARY KEY
		CLUSTERED
		([CoachatButtonID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatButtons] SET (LOCK_ESCALATION = TABLE)
GO
