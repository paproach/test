SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserMailRecipients] (
		[UserMailRecipientsID]     [int] IDENTITY(1, 1) NOT NULL,
		[MessageID]                [int] NOT NULL,
		[RecipientUserID]          [int] NOT NULL,
		[Recipiented]              [bit] NULL,
		CONSTRAINT [PK_UserMailRecipients]
		PRIMARY KEY
		CLUSTERED
		([UserMailRecipientsID], [MessageID], [RecipientUserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserMailRecipients]
	WITH CHECK
	ADD CONSTRAINT [FK_UserMailRecipients_UserMail]
	FOREIGN KEY ([MessageID]) REFERENCES [dbo].[UserMail] ([MessageID])
ALTER TABLE [dbo].[UserMailRecipients]
	CHECK CONSTRAINT [FK_UserMailRecipients_UserMail]

GO
ALTER TABLE [dbo].[UserMailRecipients]
	WITH CHECK
	ADD CONSTRAINT [FK_UserMailRecipients_Users]
	FOREIGN KEY ([RecipientUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserMailRecipients]
	CHECK CONSTRAINT [FK_UserMailRecipients_Users]

GO
ALTER TABLE [dbo].[UserMailRecipients] SET (LOCK_ESCALATION = TABLE)
GO
