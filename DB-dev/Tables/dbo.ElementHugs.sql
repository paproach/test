SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementHugs] (
		[ElementTypeID]      [int] NOT NULL,
		[ElementID]          [int] NOT NULL,
		[UserID]             [int] NOT NULL,
		[LastUpdateDate]     [datetime] NULL,
		CONSTRAINT [PK_ElementHugs]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementHugs]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementHugs_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[ElementHugs]
	CHECK CONSTRAINT [FK_ElementHugs_ElementTypes]

GO
ALTER TABLE [dbo].[ElementHugs]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementHugs_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ElementHugs]
	CHECK CONSTRAINT [FK_ElementHugs_Users]

GO
ALTER TABLE [dbo].[ElementHugs] SET (LOCK_ESCALATION = TABLE)
GO
