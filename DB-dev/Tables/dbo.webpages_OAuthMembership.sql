SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership] (
		[Provider]           [nvarchar](30) COLLATE Hebrew_CI_AS NOT NULL,
		[ProviderUserId]     [nvarchar](100) COLLATE Hebrew_CI_AS NOT NULL,
		[UserId]             [int] NOT NULL,
		CONSTRAINT [PK__webpages__F53FC0ED3F1C4B12]
		PRIMARY KEY
		CLUSTERED
		([Provider], [ProviderUserId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_OAuthMembership] SET (LOCK_ESCALATION = TABLE)
GO
