SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentPolicyOptions] (
		[PaymentPolicyID]     [int] NOT NULL,
		[PaymentPolicy]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblCommunityPaymentPolicy]
		PRIMARY KEY
		CLUSTERED
		([PaymentPolicyID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PaymentPolicyOptions] SET (LOCK_ESCALATION = TABLE)
GO
