SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hug] (
		[UserHugID]     [int] IDENTITY(1, 1) NOT NULL,
		[HugID]         [int] NULL,
		[HugName]       [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[CountHug]      [int] NULL,
		CONSTRAINT [PK_Hug]
		PRIMARY KEY
		CLUSTERED
		([UserHugID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Hug] SET (LOCK_ESCALATION = TABLE)
GO
