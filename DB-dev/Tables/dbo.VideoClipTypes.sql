SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoClipTypes] (
		[ClipTypeID]     [int] NOT NULL,
		[ClipType]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_VideoClipTypes]
		PRIMARY KEY
		CLUSTERED
		([ClipTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoClipTypes] SET (LOCK_ESCALATION = TABLE)
GO
