SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TagCategories] (
		[TagCategoryID]     [int] IDENTITY(1, 1) NOT NULL,
		[TagCategory]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblTagCategories]
		PRIMARY KEY
		CLUSTERED
		([TagCategoryID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TagCategories] SET (LOCK_ESCALATION = TABLE)
GO
