SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AspNetUserLogins] (
		[LoginProvider]     [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[ProviderKey]       [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[UserId]            [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_dbo.AspNetUserLogins]
		PRIMARY KEY
		CLUSTERED
		([LoginProvider], [ProviderKey], [UserId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserLogins]
	WITH NOCHECK
	ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserLogins]
	CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]

GO
ALTER TABLE [dbo].[AspNetUserLogins] SET (LOCK_ESCALATION = TABLE)
GO
