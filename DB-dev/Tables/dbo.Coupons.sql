SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupons] (
		[CouponID]                [int] IDENTITY(1, 1) NOT NULL,
		[CreationTimestamp]       [datetime] NULL,
		[CreatorUserID]           [int] NULL,
		[Name]                    [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Description]             [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[ValidFrom]               [datetime] NULL,
		[ValidTo]                 [datetime] NULL,
		[IsSingleUsageCoupon]     [bit] NULL,
		[DiscountPercentage]      [int] NULL,
		[DiscountDays]            [int] NULL,
		[IsPaidByCustomer]        [bit] NULL,
		[PayingUserID]            [int] NULL,
		[CouponCode]              [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[UsedByUserID]            [int] NULL,
		[UsageTimestamp]          [datetime] NULL,
		[CustomerInvoice]         [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[CouponByRequest]         [bit] NULL,
		CONSTRAINT [PK_Coupons]
		PRIMARY KEY
		CLUSTERED
		([CouponID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Coupons] SET (LOCK_ESCALATION = TABLE)
GO
