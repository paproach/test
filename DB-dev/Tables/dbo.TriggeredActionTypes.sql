SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TriggeredActionTypes] (
		[TriggeredActionTypeID]     [int] IDENTITY(1, 1) NOT NULL,
		[TriggeredActionType]       [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblTriggeredActionTypes]
		PRIMARY KEY
		CLUSTERED
		([TriggeredActionTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TriggeredActionTypes] SET (LOCK_ESCALATION = TABLE)
GO
