SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserContent] (
		[ContentID]                [int] NOT NULL,
		[UserID]                   [int] NOT NULL,
		[LastDate]                 [datetime] NULL,
		[FirstTimeAfterUpdate]     [datetime] NULL,
		CONSTRAINT [PK_UserContent]
		PRIMARY KEY
		CLUSTERED
		([ContentID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserContent]
	WITH CHECK
	ADD CONSTRAINT [FK_UserContent_Content]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[UserContent]
	CHECK CONSTRAINT [FK_UserContent_Content]

GO
ALTER TABLE [dbo].[UserContent]
	WITH CHECK
	ADD CONSTRAINT [FK_UserContent_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserContent]
	CHECK CONSTRAINT [FK_UserContent_Users]

GO
ALTER TABLE [dbo].[UserContent] SET (LOCK_ESCALATION = TABLE)
GO
