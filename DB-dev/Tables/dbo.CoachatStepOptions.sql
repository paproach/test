SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatStepOptions] (
		[CoachatStepOptionID]     [int] IDENTITY(1, 1) NOT NULL,
		[CoachatStepID]           [int] NULL,
		[CoachatID]               [int] NULL,
		[OptionText]              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[NextCoachatStepID]       [int] NULL,
		[NextCoachatID]           [int] NULL,
		CONSTRAINT [PK_CoachatStepOptions]
		PRIMARY KEY
		CLUSTERED
		([CoachatStepOptionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatStepOptions]
	WITH CHECK
	ADD CONSTRAINT [FK_CoachatStepOptions_Coachats]
	FOREIGN KEY ([CoachatID]) REFERENCES [dbo].[Coachats] ([CoachatID])
ALTER TABLE [dbo].[CoachatStepOptions]
	CHECK CONSTRAINT [FK_CoachatStepOptions_Coachats]

GO
ALTER TABLE [dbo].[CoachatStepOptions]
	WITH CHECK
	ADD CONSTRAINT [FK_CoachatStepOptions_CoachatSteps]
	FOREIGN KEY ([CoachatStepID]) REFERENCES [dbo].[CoachatSteps] ([CoachatStepID])
ALTER TABLE [dbo].[CoachatStepOptions]
	CHECK CONSTRAINT [FK_CoachatStepOptions_CoachatSteps]

GO
ALTER TABLE [dbo].[CoachatStepOptions] SET (LOCK_ESCALATION = TABLE)
GO
