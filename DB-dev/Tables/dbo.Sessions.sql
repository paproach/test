SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sessions] (
		[CourseID]               [int] NULL,
		[SessionID]              [int] IDENTITY(1, 1) NOT NULL,
		[SessionNumber]          [int] NULL,
		[Name]                   [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Image]                  [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Description]            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[PrerollVideoID]         [int] NULL,
		[VideoID]                [int] NULL,
		[PostrollVideoID]        [int] NULL,
		[IntroVideoID]           [int] NULL,
		[InspirationVideoID]     [int] NULL,
		[IntroText]              [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[InspirationText]        [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ActionItemID]           [int] NULL,
		[RelatedContentID1]      [int] NULL,
		[RelatedContentID2]      [int] NULL,
		[RelatedContentID3]      [int] NULL,
		[CreatorUserID]          [int] NULL,
		[Link1]                  [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Link2]                  [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Link3]                  [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[CreationDate]           [datetime] NULL,
		[SourceCopy]             [int] NULL,
		CONSTRAINT [PK_SpecialForceSessions]
		PRIMARY KEY
		CLUSTERED
		([SessionID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sessions_VideoClipsInspiration]
	FOREIGN KEY ([InspirationVideoID]) REFERENCES [dbo].[VideoClips] ([VideoClipID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_Sessions_VideoClipsInspiration]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_Sessions_VideoClipsIntro]
	FOREIGN KEY ([IntroVideoID]) REFERENCES [dbo].[VideoClips] ([VideoClipID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_Sessions_VideoClipsIntro]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceSessions_CreatorUser]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_SpecialForceSessions_CreatorUser]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceSessions_Postroll]
	FOREIGN KEY ([PostrollVideoID]) REFERENCES [dbo].[VideoClips] ([VideoClipID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_SpecialForceSessions_Postroll]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceSessions_Preroll]
	FOREIGN KEY ([PrerollVideoID]) REFERENCES [dbo].[VideoClips] ([VideoClipID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_SpecialForceSessions_Preroll]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceSessions_SpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_SpecialForceSessions_SpecialForces]

GO
ALTER TABLE [dbo].[Sessions]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceSessions_VideoClip]
	FOREIGN KEY ([VideoID]) REFERENCES [dbo].[VideoClips] ([VideoClipID])
ALTER TABLE [dbo].[Sessions]
	CHECK CONSTRAINT [FK_SpecialForceSessions_VideoClip]

GO
ALTER TABLE [dbo].[Sessions] SET (LOCK_ESCALATION = TABLE)
GO
