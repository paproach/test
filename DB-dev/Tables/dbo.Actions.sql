SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actions] (
		[ActionID]                     [int] IDENTITY(1, 1) NOT NULL,
		[ActionTypeID]                 [int] NULL,
		[CourseID]                     [int] NULL,
		[ActionImage]                  [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[ActionDesc]                   [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ActionBody]                   [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ActionBody2]                  [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ActionPreviewVideo]           [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[ActionBodyVideo]              [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[PartTwoActionID]              [int] NULL,
		[ActionTips]                   [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[CreatorUserID]                [int] NULL,
		[CreationDate]                 [datetime] NULL,
		[LastUpdateDate]               [datetime] NULL,
		[PublishingDate]               [datetime] NULL,
		[ActionDate]                   [datetime] NULL,
		[SuccessRate]                  [int] NULL,
		[PaymentPolicyID]              [int] NULL,
		[viewCount]                    [int] NULL,
		[HugsCount]                    [int] NULL,
		[Icon]                         [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[ActionTitle]                  [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Scheduled]                    [bit] NULL,
		[ScheduledDate]                [datetime] NULL,
		[EndDate]                      [datetime] NULL,
		[IsRecurrent]                  [bit] NULL,
		[RecurrencyDurationInDays]     [int] NULL,
		[RecurrencyUnitsID]            [int] NULL,
		[RecurrenncyWeekDays]          [nvarchar](7) COLLATE Hebrew_CI_AS NULL,
		[ActionTimeOfDay]              [time](7) NULL,
		[NameAnswer0]                  [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[NameAnswer1]                  [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[ActionLinked1]                [int] NULL,
		[ActionLinked2]                [int] NULL,
		[SourceCopy]                   [int] NULL,
		CONSTRAINT [PK_TblSpecialForceActions]
		PRIMARY KEY
		CLUSTERED
		([ActionID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_ActionDate]
	DEFAULT ('1900-01-01 00:00:00.000') FOR [ActionDate]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_CreationDate]
	DEFAULT ('1900-01-01 00:00:00.000') FOR [CreationDate]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_CreatorUserID]
	DEFAULT ((1)) FOR [CreatorUserID]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_LastUpdateDate]
	DEFAULT ('1900-01-01 00:00:00.000') FOR [LastUpdateDate]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_PaymentPolicyID]
	DEFAULT ((0)) FOR [PaymentPolicyID]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_PublishingDate]
	DEFAULT ('1900-01-01 00:00:00.000') FOR [PublishingDate]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_SpecialForceID]
	DEFAULT ((1)) FOR [CourseID]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_SuccessRate]
	DEFAULT ((0)) FOR [SuccessRate]
GO
ALTER TABLE [dbo].[Actions]
	ADD
	CONSTRAINT [DF_SpecialForceActions_viewCount]
	DEFAULT ((0)) FOR [viewCount]
GO
ALTER TABLE [dbo].[Actions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_SpecialForceActions_ActionType]
	FOREIGN KEY ([ActionTypeID]) REFERENCES [dbo].[ActionType] ([ActionTypeID])
ALTER TABLE [dbo].[Actions]
	CHECK CONSTRAINT [FK_SpecialForceActions_ActionType]

GO
ALTER TABLE [dbo].[Actions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_SpecialForceActions_SpecialForceActions]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Actions]
	CHECK CONSTRAINT [FK_SpecialForceActions_SpecialForceActions]

GO
ALTER TABLE [dbo].[Actions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_SpecialForceActions_SpecialForceActions1]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[Actions]
	CHECK CONSTRAINT [FK_SpecialForceActions_SpecialForceActions1]

GO
ALTER TABLE [dbo].[Actions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_SpecialForceActions_SpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[Actions]
	CHECK CONSTRAINT [FK_SpecialForceActions_SpecialForces]

GO
EXEC sp_addextendedproperty N'MS_Description', N'1=Daily; 2=Weekly; 3=Monthly; 4=Yearly', 'SCHEMA', N'dbo', 'TABLE', N'Actions', 'COLUMN', N'RecurrencyUnitsID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'XXXXXXX - each X refers to a day. 0 means no, 1 means yes. For example, on mondays and wednesdays: 0101000', 'SCHEMA', N'dbo', 'TABLE', N'Actions', 'COLUMN', N'RecurrenncyWeekDays'
GO
ALTER TABLE [dbo].[Actions] SET (LOCK_ESCALATION = TABLE)
GO
