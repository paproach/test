SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentTopListItems] (
		[ContentID]          [int] NULL,
		[TopListID]          [int] IDENTITY(1, 1) NOT NULL,
		[Title]              [nvarchar](2000) COLLATE Hebrew_CI_AS NULL,
		[Body]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[CreatorUserID]      [int] NULL,
		[Date]               [datetime] NULL,
		[ViewCount]          [int] NULL,
		[HugsCount]          [int] NULL,
		[StatusApproval]     [int] NULL,
		[UserAnonymous]      [bit] NULL,
		CONSTRAINT [PK_TblContentTopListItems]
		PRIMARY KEY
		CLUSTERED
		([TopListID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentTopListItems]
	ADD
	CONSTRAINT [DF_ContentTopListItems_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[ContentTopListItems]
	ADD
	CONSTRAINT [DF_ContentTopListItems_ViewCount]
	DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[ContentTopListItems]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentTopListItems_Users]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ContentTopListItems]
	CHECK CONSTRAINT [FK_ContentTopListItems_Users]

GO
ALTER TABLE [dbo].[ContentTopListItems]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentTopListItems_TblContent]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ContentTopListItems]
	CHECK CONSTRAINT [FK_TblContentTopListItems_TblContent]

GO
ALTER TABLE [dbo].[ContentTopListItems] SET (LOCK_ESCALATION = TABLE)
GO
