SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SessionsHoursView] (
		[SessionsHoursViewID]     [int] IDENTITY(1, 1) NOT NULL,
		[NumberSession]           [int] NULL,
		[NumberWeek]              [int] NULL,
		[DayWeek]                 [int] NULL,
		[HourStart]               [time](7) NULL,
		[HourEnd]                 [time](7) NULL,
		CONSTRAINT [PK_SessionsHoursView]
		PRIMARY KEY
		CLUSTERED
		([SessionsHoursViewID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SessionsHoursView] SET (LOCK_ESCALATION = TABLE)
GO
