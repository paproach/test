SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoLoops] (
		[LoopID]                [int] IDENTITY(1, 1) NOT NULL,
		[LoopName]              [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[DurationInMinutes]     [int] NULL,
		[CommunityID]           [int] NULL,
		CONSTRAINT [PK_VideoLoops]
		PRIMARY KEY
		CLUSTERED
		([LoopID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoLoops] SET (LOCK_ESCALATION = TABLE)
GO
