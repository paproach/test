SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles] (
		[RoleID]              [int] NOT NULL,
		[DescriptionRole]     [nvarchar](20) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblRoles]
		PRIMARY KEY
		CLUSTERED
		([RoleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roles] SET (LOCK_ESCALATION = TABLE)
GO
