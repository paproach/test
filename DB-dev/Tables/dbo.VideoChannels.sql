SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoChannels] (
		[VideoChannelID]     [int] IDENTITY(1, 1) NOT NULL,
		[CommunityID]        [int] NULL,
		[Name]               [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Slogan]             [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[LoopID]             [int] NULL,
		CONSTRAINT [PK_VideoChannels]
		PRIMARY KEY
		CLUSTERED
		([VideoChannelID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoChannels] SET (LOCK_ESCALATION = TABLE)
GO
