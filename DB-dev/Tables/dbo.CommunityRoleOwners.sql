SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CommunityRoleOwners] (
		[CommunityID]     [int] NOT NULL,
		[UserID]          [int] NOT NULL,
		[RoleID]          [int] NOT NULL,
		CONSTRAINT [PK_TblCommunityRoleOwners]
		PRIMARY KEY
		CLUSTERED
		([CommunityID], [UserID], [RoleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunityRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunityRoleOwners_TblCommunities]
	FOREIGN KEY ([CommunityID]) REFERENCES [dbo].[Communities] ([CommunityID])
ALTER TABLE [dbo].[CommunityRoleOwners]
	CHECK CONSTRAINT [FK_TblCommunityRoleOwners_TblCommunities]

GO
ALTER TABLE [dbo].[CommunityRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunityRoleOwners_TblRoles]
	FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID])
ALTER TABLE [dbo].[CommunityRoleOwners]
	CHECK CONSTRAINT [FK_TblCommunityRoleOwners_TblRoles]

GO
ALTER TABLE [dbo].[CommunityRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblCommunityRoleOwners_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[CommunityRoleOwners]
	CHECK CONSTRAINT [FK_TblCommunityRoleOwners_TblUsers]

GO
ALTER TABLE [dbo].[CommunityRoleOwners] SET (LOCK_ESCALATION = TABLE)
GO
