SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserDailyReport] (
		[UserDailyReportID]        [int] IDENTITY(1, 1) NOT NULL,
		[UserID]                   [int] NOT NULL,
		[DateDailyReport]          [date] NOT NULL,
		[AnswerSurveyQuestion]     [int] NULL,
		CONSTRAINT [PK_UserDailyReport]
		PRIMARY KEY
		CLUSTERED
		([UserID], [DateDailyReport])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserDailyReport]
	ADD
	CONSTRAINT [DF_UserDailyReport_AnswerSurveyQuestion]
	DEFAULT ((0)) FOR [AnswerSurveyQuestion]
GO
ALTER TABLE [dbo].[UserDailyReport]
	WITH CHECK
	ADD CONSTRAINT [FK_UserDailyReport_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserDailyReport]
	CHECK CONSTRAINT [FK_UserDailyReport_Users]

GO
ALTER TABLE [dbo].[UserDailyReport] SET (LOCK_ESCALATION = TABLE)
GO
