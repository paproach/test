SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ContentDebateArgsVotes] (
		[DebateArgsVotesID]     [int] IDENTITY(1, 1) NOT NULL,
		[DebateArgID]           [int] NOT NULL,
		[UserID]                [int] NOT NULL,
		[IsAgree]               [bit] NULL,
		CONSTRAINT [PK_ContentDebateArgsVotes]
		PRIMARY KEY
		CLUSTERED
		([DebateArgID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentDebateArgsVotes]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentDebateArgsVotes_TblContentDebateArgs]
	FOREIGN KEY ([DebateArgID]) REFERENCES [dbo].[ContentDebateArgs] ([DebateArgID])
ALTER TABLE [dbo].[ContentDebateArgsVotes]
	CHECK CONSTRAINT [FK_TblContentDebateArgsVotes_TblContentDebateArgs]

GO
ALTER TABLE [dbo].[ContentDebateArgsVotes]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContentDebateArgsVotes_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ContentDebateArgsVotes]
	CHECK CONSTRAINT [FK_TblContentDebateArgsVotes_TblUsers]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Agree=True/1 , DisAgree=False/0', 'SCHEMA', N'dbo', 'TABLE', N'ContentDebateArgsVotes', 'COLUMN', N'IsAgree'
GO
ALTER TABLE [dbo].[ContentDebateArgsVotes] SET (LOCK_ESCALATION = TABLE)
GO
