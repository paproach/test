SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TriggeredActionsLog] (
		[TriggeredActionID]     [int] IDENTITY(1, 1) NOT NULL,
		[TriggerID]             [int] NULL,
		[ActionID]              [int] NULL,
		[TriggerTimestamp]      [datetime] NULL,
		[Count]                 [int] NULL,
		[UserID]                [int] NULL,
		[ContentID]             [int] NULL,
		[ContentTypeID]         [int] NULL,
		[SubContentID]          [int] NULL,
		[CommunityID]           [int] NULL,
		[SpecialForceID]        [int] NULL,
		CONSTRAINT [PK_TblTriggeredActions]
		PRIMARY KEY
		CLUSTERED
		([TriggeredActionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TriggeredActionsLog] SET (LOCK_ESCALATION = TABLE)
GO
