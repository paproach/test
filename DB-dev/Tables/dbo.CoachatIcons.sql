SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatIcons] (
		[CoachatIconID]       [int] IDENTITY(1, 1) NOT NULL,
		[CoachatIconURL]      [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[CoachatIconName]     [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_CoachatIcons]
		PRIMARY KEY
		CLUSTERED
		([CoachatIconID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatIcons] SET (LOCK_ESCALATION = TABLE)
GO
