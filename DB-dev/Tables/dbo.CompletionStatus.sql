SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompletionStatus] (
		[CompletionStatusID]     [int] NOT NULL,
		[CompletionStatus]       [nchar](20) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_CompletionStatus]
		PRIMARY KEY
		CLUSTERED
		([CompletionStatusID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CompletionStatus] SET (LOCK_ESCALATION = TABLE)
GO
