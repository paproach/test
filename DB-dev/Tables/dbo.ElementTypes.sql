SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElementTypes] (
		[ElementTypeID]       [int] IDENTITY(1, 1) NOT NULL,
		[ElementTypeDesc]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblElementTypes]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementTypes] SET (LOCK_ESCALATION = TABLE)
GO
