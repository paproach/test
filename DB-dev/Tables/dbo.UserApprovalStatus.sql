SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserApprovalStatus] (
		[ApprovalStatusID]     [int] IDENTITY(1, 1) NOT NULL,
		[UserID]               [int] NULL,
		[ElementTypeID]        [int] NULL,
		[ElementID]            [int] NULL,
		[ChangeDateTime]       [datetime] NULL,
		[StatusID]             [int] NULL,
		CONSTRAINT [PK_UserApprovalStatus]
		PRIMARY KEY
		CLUSTERED
		([ApprovalStatusID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserApprovalStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserApprovalStatus_ApprovalStatus]
	FOREIGN KEY ([StatusID]) REFERENCES [dbo].[ApprovalStatus] ([StatusID])
ALTER TABLE [dbo].[UserApprovalStatus]
	CHECK CONSTRAINT [FK_UserApprovalStatus_ApprovalStatus]

GO
ALTER TABLE [dbo].[UserApprovalStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserApprovalStatus_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[UserApprovalStatus]
	CHECK CONSTRAINT [FK_UserApprovalStatus_ElementTypes]

GO
ALTER TABLE [dbo].[UserApprovalStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserApprovalStatus_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserApprovalStatus]
	CHECK CONSTRAINT [FK_UserApprovalStatus_Users]

GO
ALTER TABLE [dbo].[UserApprovalStatus] SET (LOCK_ESCALATION = TABLE)
GO
