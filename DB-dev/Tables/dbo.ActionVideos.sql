SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionVideos] (
		[ActionID]     [int] NOT NULL,
		[Video]        [nvarchar](200) COLLATE Hebrew_CI_AS NOT NULL,
		[Body]         [nvarchar](500) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_ActionVideos]
		PRIMARY KEY
		CLUSTERED
		([ActionID], [Video])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionVideos]
	WITH CHECK
	ADD CONSTRAINT [FK_ActionVideos_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionVideos]
	CHECK CONSTRAINT [FK_ActionVideos_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionVideos] SET (LOCK_ESCALATION = TABLE)
GO
