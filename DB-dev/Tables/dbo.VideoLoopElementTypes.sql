SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoLoopElementTypes] (
		[VideoLoopElementID]     [int] NOT NULL,
		[VideoLoopElement]       [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[ExtraData]              [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_VideoLoopElementTypes]
		PRIMARY KEY
		CLUSTERED
		([VideoLoopElementID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoLoopElementTypes] SET (LOCK_ESCALATION = TABLE)
GO
