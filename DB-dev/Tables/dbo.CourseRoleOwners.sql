SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CourseRoleOwners] (
		[CourseID]     [int] NOT NULL,
		[UserID]       [int] NOT NULL,
		[RoleID]       [int] NOT NULL,
		CONSTRAINT [PK_TblSpecialForceMemberRoles]
		PRIMARY KEY
		CLUSTERED
		([CourseID], [UserID], [RoleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CourseRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForceRoleOwners_TblRoles]
	FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID])
ALTER TABLE [dbo].[CourseRoleOwners]
	CHECK CONSTRAINT [FK_TblSpecialForceRoleOwners_TblRoles]

GO
ALTER TABLE [dbo].[CourseRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForceRoleOwners_TblSpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[CourseRoleOwners]
	CHECK CONSTRAINT [FK_TblSpecialForceRoleOwners_TblSpecialForces]

GO
ALTER TABLE [dbo].[CourseRoleOwners]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForceRoleOwners_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[CourseRoleOwners]
	CHECK CONSTRAINT [FK_TblSpecialForceRoleOwners_TblUsers]

GO
ALTER TABLE [dbo].[CourseRoleOwners] SET (LOCK_ESCALATION = TABLE)
GO
