SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ActionQueResultsQueNumbers] (
		[QuestionsNumberID]     [int] IDENTITY(1, 1) NOT NULL,
		[ActionID]              [int] NULL,
		[QuestionsNumber]       [int] NULL,
		[GroupNumber]           [int] NULL,
		CONSTRAINT [PK_ActionQueResultsQueNumbers]
		PRIMARY KEY
		CLUSTERED
		([QuestionsNumberID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQueResultsQueNumbers]
	WITH CHECK
	ADD CONSTRAINT [FK_ActionQueResultsQueNumbers_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionQueResultsQueNumbers]
	CHECK CONSTRAINT [FK_ActionQueResultsQueNumbers_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionQueResultsQueNumbers] SET (LOCK_ESCALATION = TABLE)
GO
