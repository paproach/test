SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserPage] (
		[PageTitle]     [nvarchar](150) COLLATE Hebrew_CI_AS NOT NULL,
		[UserID]        [int] NOT NULL,
		[LastDate]      [datetime] NULL,
		CONSTRAINT [PK_UserPage]
		PRIMARY KEY
		CLUSTERED
		([PageTitle], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPage]
	WITH CHECK
	ADD CONSTRAINT [FK_UserPage_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserPage]
	CHECK CONSTRAINT [FK_UserPage_Users]

GO
ALTER TABLE [dbo].[UserPage] SET (LOCK_ESCALATION = TABLE)
GO
