SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoachatSteps] (
		[CoachatStepID]                                 [int] IDENTITY(1, 1) NOT NULL,
		[CoachatID]                                     [int] NULL,
		[PreviousStepAnswer]                            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Question]                                      [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[ProposedLinkName]                              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[ProposedLinkURL]                               [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[RedirectToCoachatID]                           [int] NULL,
		[ParentCoachatStepID]                           [int] NULL,
		[IsCoachatCompletionStep]                       [bit] NULL,
		[CoachatButtonID]                               [int] NULL,
		[IsSystemQuestion]                              [bit] NULL,
		[CoachatSystemQuestionID]                       [int] NULL,
		[IsSystemAnswer]                                [bit] NULL,
		[PreviousStepCoachatSystemQuestionAnswerID]     [int] NULL,
		[CoachatIconID]                                 [int] NULL,
		CONSTRAINT [PK_CoachatSteps]
		PRIMARY KEY
		CLUSTERED
		([CoachatStepID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoachatSteps]
	WITH CHECK
	ADD CONSTRAINT [FK_CoachatSteps_Coachats]
	FOREIGN KEY ([CoachatID]) REFERENCES [dbo].[Coachats] ([CoachatID])
ALTER TABLE [dbo].[CoachatSteps]
	CHECK CONSTRAINT [FK_CoachatSteps_Coachats]

GO
ALTER TABLE [dbo].[CoachatSteps] SET (LOCK_ESCALATION = TABLE)
GO
