SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Languages] (
		[LangID]       [varchar](2) COLLATE Hebrew_CI_AS NOT NULL,
		[Language]     [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblLanguages]
		PRIMARY KEY
		CLUSTERED
		([LangID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Languages] SET (LOCK_ESCALATION = TABLE)
GO
