SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories] (
		[CategoryID]           [int] IDENTITY(1, 1) NOT NULL,
		[CategoryName]         [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[ParentCategoryID]     [int] NULL,
		CONSTRAINT [PK_Categories]
		PRIMARY KEY
		CLUSTERED
		([CategoryID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Categories] SET (LOCK_ESCALATION = TABLE)
GO
