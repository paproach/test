SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoClips] (
		[VideoClipID]             [int] IDENTITY(1, 1) NOT NULL,
		[WebsiteID]               [int] NULL,
		[URL]                     [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Name]                    [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Thumbnail]               [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Duration]                [int] NULL,
		[Author]                  [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[CommunityID]             [int] NULL,
		[CreationDate]            [datetime] NULL,
		[ViewCount]               [int] NULL,
		[Likes]                   [int] NULL,
		[Dislikes]                [int] NULL,
		[PrerollVideoClipID]      [int] NULL,
		[PostrollVideoClipID]     [int] NULL,
		[ClipTypeID]              [int] NULL,
		CONSTRAINT [PK_VideoClips]
		PRIMARY KEY
		CLUSTERED
		([VideoClipID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoClips]
	ADD
	CONSTRAINT [DF_VideoClips_Dislikes]
	DEFAULT ((0)) FOR [Dislikes]
GO
ALTER TABLE [dbo].[VideoClips]
	ADD
	CONSTRAINT [DF_VideoClips_Likes]
	DEFAULT ((0)) FOR [Likes]
GO
ALTER TABLE [dbo].[VideoClips]
	ADD
	CONSTRAINT [DF_VideoClips_ViewCount]
	DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[VideoClips]
	WITH CHECK
	ADD CONSTRAINT [FK_VideoClips_VideoClipTypes]
	FOREIGN KEY ([ClipTypeID]) REFERENCES [dbo].[VideoClipTypes] ([ClipTypeID])
ALTER TABLE [dbo].[VideoClips]
	CHECK CONSTRAINT [FK_VideoClips_VideoClipTypes]

GO
ALTER TABLE [dbo].[VideoClips] SET (LOCK_ESCALATION = TABLE)
GO
