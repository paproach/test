SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Content] (
		[ContentID]          [int] IDENTITY(1, 1) NOT NULL,
		[ContentTypeID]      [int] NULL,
		[ContentType2]       [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[CreatorUserID]      [int] NULL,
		[Title]              [nvarchar](300) COLLATE Hebrew_CI_AS NULL,
		[Body]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Image]              [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[CreationDate]       [datetime] NULL,
		[LastUpdateDate]     [datetime] NULL,
		[PublishingDate]     [datetime] NULL,
		[Link]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Link2]              [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[viewCount]          [int] NULL,
		[LastViewDate]       [datetime] NULL,
		[HugsCount]          [int] NULL,
		[StatusApproval]     [int] NULL,
		[Favorite]           [int] NULL,
		[AuthorBook]         [nvarchar](300) COLLATE Hebrew_CI_AS NULL,
		[Answer1]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Answer0]            [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[IsPlatformItem]     [bit] NULL,
		[IsFeatureItem]      [bit] NULL,
		[SourceCopy]         [int] NULL,
		CONSTRAINT [PK_TblContent]
		PRIMARY KEY
		CLUSTERED
		([ContentID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Content]
	ADD
	CONSTRAINT [DF_Content_Favorite]
	DEFAULT ((99)) FOR [Favorite]
GO
ALTER TABLE [dbo].[Content]
	ADD
	CONSTRAINT [DF_Content_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[Content]
	ADD
	CONSTRAINT [DF_Content_Image]
	DEFAULT ('/Content/Images/SpecialForce.jpg') FOR [Image]
GO
ALTER TABLE [dbo].[Content]
	ADD
	CONSTRAINT [DF_Content_StatusApproval]
	DEFAULT ((1)) FOR [StatusApproval]
GO
ALTER TABLE [dbo].[Content]
	ADD
	CONSTRAINT [DF_Content_viewCount]
	DEFAULT ((0)) FOR [viewCount]
GO
ALTER TABLE [dbo].[Content]
	WITH CHECK
	ADD CONSTRAINT [FK_Content_StatusApproval]
	FOREIGN KEY ([StatusApproval]) REFERENCES [dbo].[CommunityStatusOptions] ([CommunityStatusID])
ALTER TABLE [dbo].[Content]
	CHECK CONSTRAINT [FK_Content_StatusApproval]

GO
ALTER TABLE [dbo].[Content]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContent_TblContentTypes]
	FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentTypes] ([ContentTypeID])
ALTER TABLE [dbo].[Content]
	CHECK CONSTRAINT [FK_TblContent_TblContentTypes]

GO
ALTER TABLE [dbo].[Content]
	WITH CHECK
	ADD CONSTRAINT [FK_TblContent_TblUsers]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Content]
	CHECK CONSTRAINT [FK_TblContent_TblUsers]

GO
ALTER TABLE [dbo].[Content] SET (LOCK_ESCALATION = TABLE)
GO
