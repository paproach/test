SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore] (
		[UserID]            [int] NOT NULL,
		[QuestionID]        [int] NOT NULL,
		[AnswerID]          [int] NULL,
		[ScoringAnswer]     [float] NULL,
		[Date]              [datetime] NULL,
		CONSTRAINT [PK_UserPersonalityTraitsQuestionnaireScore]
		PRIMARY KEY
		CLUSTERED
		([UserID], [QuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore]
	WITH CHECK
	ADD CONSTRAINT [FK_UserPersonalityTraitsQuestionnaireScore_PersonalityTraitsQuestionnaire]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[PersonalityTraitsQuestionnaire] ([QuestionID])
ALTER TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore]
	CHECK CONSTRAINT [FK_UserPersonalityTraitsQuestionnaireScore_PersonalityTraitsQuestionnaire]

GO
ALTER TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore]
	WITH CHECK
	ADD CONSTRAINT [FK_UserPersonalityTraitsQuestionnaireScore_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore]
	CHECK CONSTRAINT [FK_UserPersonalityTraitsQuestionnaireScore_Users]

GO
ALTER TABLE [dbo].[UserPersonalityTraitsQuestionnaireScore] SET (LOCK_ESCALATION = TABLE)
GO
