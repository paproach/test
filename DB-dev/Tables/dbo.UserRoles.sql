SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserRoles] (
		[UserID]     [int] NOT NULL,
		[RoleID]     [int] NOT NULL,
		CONSTRAINT [PK_UserRoles]
		PRIMARY KEY
		CLUSTERED
		([UserID], [RoleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserRoles]
	ADD
	CONSTRAINT [DF_UserRoles_Role]
	DEFAULT ((1)) FOR [RoleID]
GO
ALTER TABLE [dbo].[UserRoles]
	ADD
	CONSTRAINT [DF_UserRoles_UserID]
	DEFAULT ((1)) FOR [UserID]
GO
ALTER TABLE [dbo].[UserRoles]
	WITH CHECK
	ADD CONSTRAINT [FK_UserRoles_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserRoles]
	CHECK CONSTRAINT [FK_UserRoles_Users]

GO
ALTER TABLE [dbo].[UserRoles] SET (LOCK_ESCALATION = TABLE)
GO
