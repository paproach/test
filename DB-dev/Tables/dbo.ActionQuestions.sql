SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionQuestions] (
		[ActionID]             [int] NULL,
		[QuestionID]           [int] IDENTITY(1, 1) NOT NULL,
		[QuestionTypeID]       [int] NULL,
		[Question]             [nvarchar](3000) COLLATE Hebrew_CI_AS NULL,
		[IsFreeTextAnswer]     [bit] NULL,
		CONSTRAINT [PK_SpecialForceActionQuestions]
		PRIMARY KEY
		CLUSTERED
		([QuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQuestions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_SpecialForceActionQuestions_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionQuestions]
	CHECK CONSTRAINT [FK_SpecialForceActionQuestions_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionQuestions] SET (LOCK_ESCALATION = TABLE)
GO
