SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserSpamStatus] (
		[UserSpamStatusID]     [int] IDENTITY(1, 1) NOT NULL,
		[ApplicantUserID]      [int] NULL,
		[ElementTypeID]        [int] NULL,
		[ElementID]            [int] NULL,
		[DateTime]             [datetime] NULL,
		[StatusID]             [int] NULL,
		[ID]                   [int] NULL,
		CONSTRAINT [PK_UserSpamStatus]
		PRIMARY KEY
		CLUSTERED
		([UserSpamStatusID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSpamStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSpamStatus_ApprovalStatus]
	FOREIGN KEY ([StatusID]) REFERENCES [dbo].[ApprovalStatus] ([StatusID])
ALTER TABLE [dbo].[UserSpamStatus]
	CHECK CONSTRAINT [FK_UserSpamStatus_ApprovalStatus]

GO
ALTER TABLE [dbo].[UserSpamStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSpamStatus_ElementTypes]
	FOREIGN KEY ([ElementTypeID]) REFERENCES [dbo].[ElementTypes] ([ElementTypeID])
ALTER TABLE [dbo].[UserSpamStatus]
	CHECK CONSTRAINT [FK_UserSpamStatus_ElementTypes]

GO
ALTER TABLE [dbo].[UserSpamStatus]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSpamStatus_Users]
	FOREIGN KEY ([ApplicantUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserSpamStatus]
	CHECK CONSTRAINT [FK_UserSpamStatus_Users]

GO
ALTER TABLE [dbo].[UserSpamStatus] SET (LOCK_ESCALATION = TABLE)
GO
