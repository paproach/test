SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempEmail] (
		[TempEmailID]     [int] IDENTITY(1, 1) NOT NULL,
		[Email]           [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Date]            [datetime] NULL,
		CONSTRAINT [PK_TempEmail]
		PRIMARY KEY
		CLUSTERED
		([TempEmailID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempEmail] SET (LOCK_ESCALATION = TABLE)
GO
