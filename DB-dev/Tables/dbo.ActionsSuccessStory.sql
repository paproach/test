SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionsSuccessStory] (
		[ActionID]         [int] NOT NULL,
		[UserID]           [int] NOT NULL,
		[SuccessStory]     [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblSpecialForceActionsSuccessStory]
		PRIMARY KEY
		CLUSTERED
		([ActionID], [UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionsSuccessStory]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForceActionsSuccessStory_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ActionsSuccessStory]
	CHECK CONSTRAINT [FK_TblSpecialForceActionsSuccessStory_TblUsers]

GO
ALTER TABLE [dbo].[ActionsSuccessStory]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForceActionsSuccessStory_TblUsers1]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ActionsSuccessStory]
	CHECK CONSTRAINT [FK_TblSpecialForceActionsSuccessStory_TblUsers1]

GO
ALTER TABLE [dbo].[ActionsSuccessStory] SET (LOCK_ESCALATION = TABLE)
GO
