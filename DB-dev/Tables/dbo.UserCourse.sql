SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserCourse] (
		[UserID]              [int] NOT NULL,
		[CourseID]            [int] NOT NULL,
		[IsCourseHidden]      [bit] NULL,
		[IsUserHidden]        [bit] NULL,
		[ActiveStartDate]     [datetime] NULL,
		[ActiveEndDate]       [datetime] NULL,
		[JoinStatus]          [int] NULL,
		[UserAnonymous]       [bit] NULL,
		[CourseType]          [int] NULL,
		[IsMyOwn]             [bit] NULL,
		[IsCompleted]         [bit] NULL,
		[CompletionDate]      [datetime] NULL,
		[JoinDate]            [datetime] NULL,
		CONSTRAINT [PK_TblUserSpecialForces]
		PRIMARY KEY
		CLUSTERED
		([UserID], [CourseID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserCourse]
	ADD
	CONSTRAINT [DF_UserSpecialForces_JoinDate]
	DEFAULT (((1)-(1))-(1900)) FOR [ActiveStartDate]
GO
ALTER TABLE [dbo].[UserCourse]
	ADD
	CONSTRAINT [DF_UserSpecialForces_SpecialForceID]
	DEFAULT ((1)) FOR [CourseID]
GO
ALTER TABLE [dbo].[UserCourse]
	ADD
	CONSTRAINT [DF_UserSpecialForces_StatusActivity]
	DEFAULT ('Active') FOR [JoinStatus]
GO
ALTER TABLE [dbo].[UserCourse]
	ADD
	CONSTRAINT [DF_UserSpecialForces_UserID]
	DEFAULT ((1)) FOR [UserID]
GO
ALTER TABLE [dbo].[UserCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserSpecialForces_TblSpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[UserCourse]
	CHECK CONSTRAINT [FK_TblUserSpecialForces_TblSpecialForces]

GO
ALTER TABLE [dbo].[UserCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserSpecialForces_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserCourse]
	CHECK CONSTRAINT [FK_TblUserSpecialForces_TblUsers]

GO
ALTER TABLE [dbo].[UserCourse] SET (LOCK_ESCALATION = TABLE)
GO
