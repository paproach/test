SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralPagesBlogPosts] (
		[BlogPostID]         [int] IDENTITY(1, 1) NOT NULL,
		[CreatorUserID]      [int] NULL,
		[Title]              [nvarchar](300) COLLATE Hebrew_CI_AS NULL,
		[Body]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Image]              [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[CreationDate]       [datetime] NULL,
		[LastUpdateDate]     [datetime] NULL,
		[PublishingDate]     [datetime] NULL,
		[Link]               [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Link2]              [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[viewCount]          [int] NULL,
		[LastViewDate]       [datetime] NULL,
		[HugsCount]          [int] NULL,
		[StatusApproval]     [int] NULL,
		[Favorite]           [int] NULL,
		[AuthorBook]         [nvarchar](300) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblGeneralPagesBlogPosts]
		PRIMARY KEY
		CLUSTERED
		([BlogPostID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	ADD
	CONSTRAINT [DF_GeneralPagesBlogPosts_Favorite]
	DEFAULT ((99)) FOR [Favorite]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	ADD
	CONSTRAINT [DF_GeneralPagesBlogPosts_HugsCount]
	DEFAULT ((0)) FOR [HugsCount]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	ADD
	CONSTRAINT [DF_GeneralPagesBlogPosts_Image]
	DEFAULT ('/Content/Images/SpecialForce.jpg') FOR [Image]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	ADD
	CONSTRAINT [DF_GeneralPagesBlogPosts_StatusApproval]
	DEFAULT ((1)) FOR [StatusApproval]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	ADD
	CONSTRAINT [DF_GeneralPagesBlogPosts_viewCount]
	DEFAULT ((0)) FOR [viewCount]
GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	WITH CHECK
	ADD CONSTRAINT [FK_GeneralPagesBlogPosts_Users]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[GeneralPagesBlogPosts]
	CHECK CONSTRAINT [FK_GeneralPagesBlogPosts_Users]

GO
ALTER TABLE [dbo].[GeneralPagesBlogPosts] SET (LOCK_ESCALATION = TABLE)
GO
