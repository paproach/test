SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommunityStatusOptions] (
		[CommunityStatusID]     [int] NOT NULL,
		[CommunityStatus]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblCommunityStatusID]
		PRIMARY KEY
		CLUSTERED
		([CommunityStatusID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunityStatusOptions] SET (LOCK_ESCALATION = TABLE)
GO
