SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ContentQueResultValue] (
		[QueResultValueID]      [int] IDENTITY(1, 1) NOT NULL,
		[QuestionID]            [int] NULL,
		[NumberingQuestion]     [int] NULL,
		[NumberingAnswer]       [int] NULL,
		[ValueAnswer]           [decimal](6, 2) NULL,
		CONSTRAINT [PK_ContentQueResultValue]
		PRIMARY KEY
		CLUSTERED
		([QueResultValueID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentQueResultValue] SET (LOCK_ESCALATION = TABLE)
GO
