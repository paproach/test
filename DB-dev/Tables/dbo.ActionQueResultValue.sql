SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ActionQueResultValue] (
		[QueResultValueID]      [int] IDENTITY(1, 1) NOT NULL,
		[QuestionID]            [int] NULL,
		[ActionID]              [int] NULL,
		[NumberingQuestion]     [int] NULL,
		[NumberingAnswer]       [int] NULL,
		[ValueAnswer]           [decimal](6, 2) NULL,
		CONSTRAINT [PK_ActionQueResultValue]
		PRIMARY KEY
		CLUSTERED
		([QueResultValueID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQueResultValue]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionQueResultValue_SpecialForceActionQuestions]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[ActionQuestions] ([QuestionID])
ALTER TABLE [dbo].[ActionQueResultValue]
	CHECK CONSTRAINT [FK_ActionQueResultValue_SpecialForceActionQuestions]

GO
ALTER TABLE [dbo].[ActionQueResultValue]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionQueResultValue_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionQueResultValue]
	CHECK CONSTRAINT [FK_ActionQueResultValue_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionQueResultValue] SET (LOCK_ESCALATION = TABLE)
GO
