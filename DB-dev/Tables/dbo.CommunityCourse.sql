SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CommunityCourse] (
		[CommunityID]         [int] NOT NULL,
		[CourseID]            [int] NOT NULL,
		[JoinDate]            [datetime] NULL,
		[Favorite]            [int] NULL,
		[AllActionPlan]       [bit] NULL,
		[CourseNumbering]     [int] NULL,
		CONSTRAINT [PK_CommunitySpecialFoeces]
		PRIMARY KEY
		CLUSTERED
		([CommunityID], [CourseID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunityCourse]
	ADD
	CONSTRAINT [DF_CommunitySpecialFoeces_AllActionPlan]
	DEFAULT ('true') FOR [AllActionPlan]
GO
ALTER TABLE [dbo].[CommunityCourse]
	ADD
	CONSTRAINT [DF_CommunitySpecialFoeces_Favorite]
	DEFAULT ((999)) FOR [Favorite]
GO
ALTER TABLE [dbo].[CommunityCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_CommunitySpecialFoeces_Communities]
	FOREIGN KEY ([CommunityID]) REFERENCES [dbo].[Communities] ([CommunityID])
ALTER TABLE [dbo].[CommunityCourse]
	CHECK CONSTRAINT [FK_CommunitySpecialFoeces_Communities]

GO
ALTER TABLE [dbo].[CommunityCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_CommunitySpecialFoeces_SpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[CommunityCourse]
	CHECK CONSTRAINT [FK_CommunitySpecialFoeces_SpecialForces]

GO
ALTER TABLE [dbo].[CommunityCourse] SET (LOCK_ESCALATION = TABLE)
GO
