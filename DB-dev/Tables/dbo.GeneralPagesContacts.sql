SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralPagesContacts] (
		[ContactID]            [int] IDENTITY(1, 1) NOT NULL,
		[Name]                 [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Email]                [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Mobile]               [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[Title]                [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Message]              [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[IsArchived]           [bit] NULL,
		[IsAnswered]           [bit] NULL,
		[ContactTimestamp]     [datetime] NULL,
		CONSTRAINT [PK_GeneralPagesContacts]
		PRIMARY KEY
		CLUSTERED
		([ContactID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralPagesContacts] SET (LOCK_ESCALATION = TABLE)
GO
