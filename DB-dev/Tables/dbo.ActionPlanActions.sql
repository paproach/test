SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ActionPlanActions] (
		[ActionPlanID]        [int] NOT NULL,
		[ActionID]            [int] NOT NULL,
		[Pos]                 [int] NULL,
		[IsMandatory]         [bit] NULL,
		[StartDateOffset]     [int] NULL,
		CONSTRAINT [PK_ActionPlanActions]
		PRIMARY KEY
		CLUSTERED
		([ActionPlanID], [ActionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionPlanActions]
	ADD
	CONSTRAINT [DF_ActionPlanActions_ActionPlanID]
	DEFAULT ((0)) FOR [ActionPlanID]
GO
ALTER TABLE [dbo].[ActionPlanActions]
	ADD
	CONSTRAINT [DF_ActionPlanActions_Pos]
	DEFAULT ((0)) FOR [Pos]
GO
ALTER TABLE [dbo].[ActionPlanActions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionPlanActions_ActionPlans]
	FOREIGN KEY ([ActionPlanID]) REFERENCES [dbo].[ActionPlans] ([ActionPlanID])
ALTER TABLE [dbo].[ActionPlanActions]
	CHECK CONSTRAINT [FK_ActionPlanActions_ActionPlans]

GO
ALTER TABLE [dbo].[ActionPlanActions]
	WITH NOCHECK
	ADD CONSTRAINT [FK_ActionPlanActions_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[ActionPlanActions]
	CHECK CONSTRAINT [FK_ActionPlanActions_SpecialForceActions]

GO
ALTER TABLE [dbo].[ActionPlanActions] SET (LOCK_ESCALATION = TABLE)
GO
