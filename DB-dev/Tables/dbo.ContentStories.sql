SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentStories] (
		[StoryID]            [int] IDENTITY(1, 1) NOT NULL,
		[ContentID]          [int] NULL,
		[UserID]             [int] NULL,
		[CreationDate]       [datetime] NULL,
		[Title]              [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Body]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Link]               [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[UserAnonymous]      [bit] NULL,
		[StatusApproval]     [int] NULL,
		CONSTRAINT [PK_ContentStories]
		PRIMARY KEY
		CLUSTERED
		([StoryID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentStories]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentStories_Content]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ContentStories]
	CHECK CONSTRAINT [FK_ContentStories_Content]

GO
ALTER TABLE [dbo].[ContentStories]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentStories_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ContentStories]
	CHECK CONSTRAINT [FK_ContentStories_Users]

GO
ALTER TABLE [dbo].[ContentStories] SET (LOCK_ESCALATION = TABLE)
GO
