SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserFriend] (
		[UserFriendID]     [int] IDENTITY(1, 1) NOT NULL,
		[FriendID]         [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Email]            [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[FirstName]        [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[LastName]         [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[FullName]         [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Gender]           [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[BirthDay]         [datetime] NULL,
		[Location]         [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[PictureUrl]       [nvarchar](300) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_UserFriend]
		PRIMARY KEY
		CLUSTERED
		([UserFriendID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserFriend] SET (LOCK_ESCALATION = TABLE)
GO
