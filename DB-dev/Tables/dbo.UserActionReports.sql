SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserActionReports] (
		[ReportActionID]         [int] IDENTITY(1, 1) NOT NULL,
		[ActionID]               [int] NULL,
		[ActionPlanID]           [int] NULL,
		[ReportDate]             [datetime] NULL,
		[UserID]                 [int] NULL,
		[CompletionStatusID]     [int] NULL,
		[LastUpdateDate]         [datetime] NULL,
		CONSTRAINT [PK_UserActionReports]
		PRIMARY KEY
		CLUSTERED
		([ReportActionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserActionReports]
	ADD
	CONSTRAINT [DF_UserActionReports_CompletionStatusID]
	DEFAULT ((0)) FOR [CompletionStatusID]
GO
ALTER TABLE [dbo].[UserActionReports]
	WITH CHECK
	ADD CONSTRAINT [FK_UserActionReports_ActionPlans]
	FOREIGN KEY ([ActionPlanID]) REFERENCES [dbo].[ActionPlans] ([ActionPlanID])
ALTER TABLE [dbo].[UserActionReports]
	CHECK CONSTRAINT [FK_UserActionReports_ActionPlans]

GO
ALTER TABLE [dbo].[UserActionReports]
	WITH CHECK
	ADD CONSTRAINT [FK_UserActionReports_CompletionStatus]
	FOREIGN KEY ([CompletionStatusID]) REFERENCES [dbo].[CompletionStatus] ([CompletionStatusID])
ALTER TABLE [dbo].[UserActionReports]
	CHECK CONSTRAINT [FK_UserActionReports_CompletionStatus]

GO
ALTER TABLE [dbo].[UserActionReports]
	WITH CHECK
	ADD CONSTRAINT [FK_UserActionReports_SpecialForceActions]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[UserActionReports]
	CHECK CONSTRAINT [FK_UserActionReports_SpecialForceActions]

GO
ALTER TABLE [dbo].[UserActionReports]
	WITH CHECK
	ADD CONSTRAINT [FK_UserActionReports_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserActionReports]
	CHECK CONSTRAINT [FK_UserActionReports_Users]

GO
CREATE NONCLUSTERED INDEX [IX_UserActionReports_UserID_ReportDate]
	ON [dbo].[UserActionReports] ([UserID], [ReportDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserActionReports] SET (LOCK_ESCALATION = TABLE)
GO
