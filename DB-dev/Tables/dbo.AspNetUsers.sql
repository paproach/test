SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AspNetUsers] (
		[UserID]                   [int] IDENTITY(1, 1) NOT NULL,
		[Id]                       [nvarchar](128) COLLATE Hebrew_CI_AS NOT NULL,
		[Email]                    [nvarchar](256) COLLATE Hebrew_CI_AS NULL,
		[EmailConfirmed]           [bit] NOT NULL,
		[PasswordHash]             [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[SecurityStamp]            [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[FullName]                 [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[FirstName]                [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[LastName]                 [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Gender]                   [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[BirthDay]                 [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[PictureUrl]               [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[Location]                 [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[PhoneNumber]              [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[PhoneNumberConfirmed]     [bit] NOT NULL,
		[TwoFactorEnabled]         [bit] NOT NULL,
		[LockoutEndDateUtc]        [datetime] NULL,
		[LockoutEnabled]           [bit] NOT NULL,
		[AccessFailedCount]        [int] NOT NULL,
		[UserName]                 [nvarchar](256) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_dbo.AspNetUsers]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] SET (LOCK_ESCALATION = TABLE)
GO
