SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonalityTraitsQuestionnaire] (
		[QuestionID]       [int] NOT NULL,
		[Question]         [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[Trat]             [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[OrderScoring]     [bit] NULL,
		CONSTRAINT [PK_PersonalityTraitsQuestionnaire]
		PRIMARY KEY
		CLUSTERED
		([QuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PersonalityTraitsQuestionnaire] SET (LOCK_ESCALATION = TABLE)
GO
