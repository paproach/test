SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserSession] (
		[UserSessionID]         [int] IDENTITY(1, 1) NOT NULL,
		[UserID]                [int] NULL,
		[SessionID]             [int] NULL,
		[DateStart]             [datetime] NULL,
		[DateEnd]               [datetime] NULL,
		[SessionActivityID]     [int] NULL,
		[DateUpdate]            [datetime] NULL,
		[OnMyOwn]               [bit] NULL,
		[ChatActivity]          [bit] NULL,
		CONSTRAINT [PK_UserSession]
		PRIMARY KEY
		CLUSTERED
		([UserSessionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSession]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSession_Sessions]
	FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Sessions] ([SessionID])
ALTER TABLE [dbo].[UserSession]
	CHECK CONSTRAINT [FK_UserSession_Sessions]

GO
ALTER TABLE [dbo].[UserSession]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSession_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserSession]
	CHECK CONSTRAINT [FK_UserSession_Users]

GO
ALTER TABLE [dbo].[UserSession] SET (LOCK_ESCALATION = TABLE)
GO
