SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ContentQueResultsQueNumbers] (
		[QuestionsNumberID]     [int] IDENTITY(1, 1) NOT NULL,
		[ContentID]             [int] NULL,
		[QuestionsNumber]       [int] NULL,
		[GroupNumber]           [int] NULL,
		CONSTRAINT [PK_QueResultsQueNumbers]
		PRIMARY KEY
		CLUSTERED
		([QuestionsNumberID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentQueResultsQueNumbers]
	WITH CHECK
	ADD CONSTRAINT [FK_ContentQueResultsQueNumbers_Content]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[ContentQueResultsQueNumbers]
	CHECK CONSTRAINT [FK_ContentQueResultsQueNumbers_Content]

GO
ALTER TABLE [dbo].[ContentQueResultsQueNumbers] SET (LOCK_ESCALATION = TABLE)
GO
