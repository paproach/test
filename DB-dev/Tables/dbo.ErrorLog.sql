SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorLog] (
		[ErrorLogID]           [int] IDENTITY(1, 1) NOT NULL,
		[ErrorDate]            [datetime] NULL,
		[ErrorMessage]         [nvarchar](500) COLLATE Hebrew_CI_AS NULL,
		[ExceptionMessage]     [nvarchar](500) COLLATE Hebrew_CI_AS NULL,
		[UserID]               [int] NULL,
		[CommunityID]          [int] NULL,
		[CourseID]             [int] NULL,
		[ActionID]             [int] NULL,
		[ActionPlanID]         [int] NULL,
		[ContentID]            [int] NULL,
		[FreeMessage]          [nvarchar](1000) COLLATE Hebrew_CI_AS NULL,
		[StackTrace]           [ntext] COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_ErrorLog]
		PRIMARY KEY
		CLUSTERED
		([ErrorLogID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
