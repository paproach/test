SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserCourseActions] (
		[UserID]             [int] NOT NULL,
		[ActionID]           [int] NOT NULL,
		[IsCompleted]        [bit] NULL,
		[CompletionDate]     [datetime] NULL,
		[StartDate]          [datetime] NULL,
		[ActionPlanID]       [int] NULL,
		[EndDate]            [datetime] NULL,
		[JoinStatus]         [varchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblUserActions]
		PRIMARY KEY
		CLUSTERED
		([UserID], [ActionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserCourseActions]
	ADD
	CONSTRAINT [DF_UserSpecialForceActions_ActionID]
	DEFAULT ((1)) FOR [ActionID]
GO
ALTER TABLE [dbo].[UserCourseActions]
	ADD
	CONSTRAINT [DF_UserSpecialForceActions_CompletionDate]
	DEFAULT (((1)-(1))-(1900)) FOR [CompletionDate]
GO
ALTER TABLE [dbo].[UserCourseActions]
	ADD
	CONSTRAINT [DF_UserSpecialForceActions_StartDate]
	DEFAULT (((1)-(1))-(1900)) FOR [StartDate]
GO
ALTER TABLE [dbo].[UserCourseActions]
	ADD
	CONSTRAINT [DF_UserSpecialForceActions_Status]
	DEFAULT ('Active') FOR [JoinStatus]
GO
ALTER TABLE [dbo].[UserCourseActions]
	ADD
	CONSTRAINT [DF_UserSpecialForceActions_UserID]
	DEFAULT ((1)) FOR [UserID]
GO
ALTER TABLE [dbo].[UserCourseActions]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserSpecialForceActions_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserCourseActions]
	CHECK CONSTRAINT [FK_TblUserSpecialForceActions_TblUsers]

GO
ALTER TABLE [dbo].[UserCourseActions]
	WITH CHECK
	ADD CONSTRAINT [FK_UserSpecialForceActions_ActionPlans]
	FOREIGN KEY ([ActionPlanID]) REFERENCES [dbo].[ActionPlans] ([ActionPlanID])
ALTER TABLE [dbo].[UserCourseActions]
	CHECK CONSTRAINT [FK_UserSpecialForceActions_ActionPlans]

GO
ALTER TABLE [dbo].[UserCourseActions] SET (LOCK_ESCALATION = TABLE)
GO
