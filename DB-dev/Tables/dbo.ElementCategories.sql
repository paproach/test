SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementCategories] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[CategoryID]        [int] NOT NULL,
		CONSTRAINT [PK_ElementCategories]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [CategoryID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementCategories]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementCategories_Categories]
	FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Categories] ([CategoryID])
ALTER TABLE [dbo].[ElementCategories]
	CHECK CONSTRAINT [FK_ElementCategories_Categories]

GO
ALTER TABLE [dbo].[ElementCategories] SET (LOCK_ESCALATION = TABLE)
GO
