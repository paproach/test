SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommunityLifeCycleOptions] (
		[CommunityLifeCycleID]     [int] NOT NULL,
		[CommunityLifeCycle]       [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblCommunityLifeCycle]
		PRIMARY KEY
		CLUSTERED
		([CommunityLifeCycleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunityLifeCycleOptions] SET (LOCK_ESCALATION = TABLE)
GO
