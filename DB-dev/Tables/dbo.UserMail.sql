SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMail] (
		[MessageID]            [int] IDENTITY(1, 1) NOT NULL,
		[FromUserID]           [int] NULL,
		[Title]                [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Body]                 [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[DateReceived]         [datetime] NULL,
		[ReplyToMessageID]     [int] NULL,
		[Draft]                [bit] NULL,
		CONSTRAINT [PK_TblUserInbox]
		PRIMARY KEY
		CLUSTERED
		([MessageID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserMail]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserMail_TblUsers]
	FOREIGN KEY ([FromUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserMail]
	CHECK CONSTRAINT [FK_TblUserMail_TblUsers]

GO
ALTER TABLE [dbo].[UserMail]
	WITH CHECK
	ADD CONSTRAINT [FK_UserMail_UserMail]
	FOREIGN KEY ([ReplyToMessageID]) REFERENCES [dbo].[UserMail] ([MessageID])
ALTER TABLE [dbo].[UserMail]
	CHECK CONSTRAINT [FK_UserMail_UserMail]

GO
ALTER TABLE [dbo].[UserMail] SET (LOCK_ESCALATION = TABLE)
GO
