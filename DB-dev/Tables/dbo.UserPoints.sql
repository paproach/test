SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserPoints] (
		[UserPointsID]      [int] IDENTITY(1, 1) NOT NULL,
		[UserID]            [int] NOT NULL,
		[PointTypeID]       [int] NOT NULL,
		[Timestamp]         [datetime] NOT NULL,
		[PointsAdded]       [float] NULL,
		[TotalPoints]       [float] NULL,
		[MonthlyTotal]      [float] NULL,
		[ElementTypeID]     [int] NULL,
		[ElementID]         [int] NULL,
		CONSTRAINT [PK_UserBonusPoints]
		PRIMARY KEY
		CLUSTERED
		([UserPointsID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPoints]
	WITH CHECK
	ADD CONSTRAINT [FK_TblUserBonusPoints_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[UserPoints]
	CHECK CONSTRAINT [FK_TblUserBonusPoints_TblUsers]

GO
ALTER TABLE [dbo].[UserPoints]
	WITH CHECK
	ADD CONSTRAINT [FK_UserBonusPoints_BonusPointTypes]
	FOREIGN KEY ([PointTypeID]) REFERENCES [dbo].[ScorePoints] ([ActionTypeID])
ALTER TABLE [dbo].[UserPoints]
	CHECK CONSTRAINT [FK_UserBonusPoints_BonusPointTypes]

GO
ALTER TABLE [dbo].[UserPoints] SET (LOCK_ESCALATION = TABLE)
GO
