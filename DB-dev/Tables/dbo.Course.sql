SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Course] (
		[CourseID]                                [int] IDENTITY(1, 1) NOT NULL,
		[Name]                                    [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Slogan]                                  [nvarchar](150) COLLATE Hebrew_CI_AS NULL,
		[Logo]                                    [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Icon]                                    [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[Objective1]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[Objective2]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[Objective3]                              [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[InvitationText]                          [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[InvitationVideo]                         [nvarchar](255) COLLATE Hebrew_CI_AS NULL,
		[ShortDescription]                        [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[LongDescription]                         [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[TargetAudienceDesc]                      [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[Requirements]                            [nvarchar](1024) COLLATE Hebrew_CI_AS NULL,
		[CreationDate]                            [datetime] NULL,
		[LaunchDate]                              [datetime] NULL,
		[EndDate]                                 [datetime] NULL,
		[IsShownInUserSFList]                     [bit] NULL,
		[SFIDDisclosurePolicyID]                  [bit] NULL,
		[IsGatedForce]                            [bit] NULL,
		[IsSponsored]                             [bit] NULL,
		[SponsorID]                               [int] NULL,
		[Likes]                                   [int] NULL,
		[Dislikes]                                [int] NULL,
		[IsBuddyAndSoulSF]                        [bit] NULL,
		[LeaderTypeID]                            [int] NULL,
		[MemberIDDisclosurePolicyID]              [int] NULL,
		[ContentViewPermissionsID]                [int] NULL,
		[ContentCreationPolicyID]                 [int] NULL,
		[ForumEntriesAuthorizationPolicyID]       [int] NULL,
		[RecommendationAuthorizationPolicyID]     [int] NULL,
		[IsAcceptanceCriteriaUsed]                [bit] NULL,
		[CreatorUserID]                           [int] NULL,
		[ParentCourseID]                          [int] NULL,
		[HugsCount]                               [int] NULL,
		[ViewCount]                               [int] NULL,
		[CategoryID]                              [int] NULL,
		[StatusApproval]                          [int] NULL,
		[Favorite]                                [int] NULL,
		[GatedMinAge]                             [int] NULL,
		[GatedMaxAge]                             [int] NULL,
		[GatedGenderID]                           [int] NULL,
		[GatedCommunityID]                        [int] NULL,
		[SourceCopy]                              [int] NULL,
		CONSTRAINT [PK_TblSpecialForces]
		PRIMARY KEY
		CLUSTERED
		([CourseID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_ContentCreationPolicyID]
	DEFAULT ((0)) FOR [ContentCreationPolicyID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_ContentViewPermissionsID]
	DEFAULT ((0)) FOR [ContentViewPermissionsID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_CreationDate]
	DEFAULT (((1)-(1))-(1)) FOR [CreationDate]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_CreatorUserID]
	DEFAULT ((0)) FOR [CreatorUserID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_Dislikes]
	DEFAULT ((0)) FOR [Dislikes]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_EndDate]
	DEFAULT (((1)-(1))-(1)) FOR [EndDate]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_ForumEntriesAuthorizationPolicyID]
	DEFAULT ((0)) FOR [ForumEntriesAuthorizationPolicyID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_IsAcceptanceCriteriaUsed]
	DEFAULT ((99)) FOR [IsAcceptanceCriteriaUsed]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_LaunchDate]
	DEFAULT (((1)-(1))-(1)) FOR [LaunchDate]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_LeaderTypeID]
	DEFAULT ((0)) FOR [LeaderTypeID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_Likes]
	DEFAULT ((0)) FOR [Likes]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_MemberIDDisclosurePolicyID]
	DEFAULT ((0)) FOR [MemberIDDisclosurePolicyID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_ParentSpecialForcesID]
	DEFAULT ((0)) FOR [ParentCourseID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_RecommendationAuthorizationPolicyID]
	DEFAULT ((0)) FOR [RecommendationAuthorizationPolicyID]
GO
ALTER TABLE [dbo].[Course]
	ADD
	CONSTRAINT [DF_SpecialForces_SponsorID]
	DEFAULT ((0)) FOR [SponsorID]
GO
ALTER TABLE [dbo].[Course]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForces_Categories]
	FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Categories] ([CategoryID])
ALTER TABLE [dbo].[Course]
	CHECK CONSTRAINT [FK_SpecialForces_Categories]

GO
ALTER TABLE [dbo].[Course]
	WITH CHECK
	ADD CONSTRAINT [FK_TblSpecialForces_TblUsers]
	FOREIGN KEY ([CreatorUserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Course]
	CHECK CONSTRAINT [FK_TblSpecialForces_TblUsers]

GO
EXEC sp_addextendedproperty N'MS_Description', N'1-approval, 2-Pending approval, 3-Postponed', 'SCHEMA', N'dbo', 'TABLE', N'Course', 'COLUMN', N'Favorite'
GO
ALTER TABLE [dbo].[Course] SET (LOCK_ESCALATION = TABLE)
GO
