SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CrossReferenceSurveyAndCourse] (
		[CrossReferenceID]     [int] IDENTITY(1, 1) NOT NULL,
		[ElemenyTypeID]        [int] NULL,
		[ContentID]            [int] NULL,
		[ActionID]             [int] NULL,
		[QuestionID]           [int] NULL,
		[Numbering]            [int] NULL,
		[AnswerID]             [int] NULL,
		[CourseID]             [int] NULL,
		CONSTRAINT [PK_CrossReferenceSurveyAndCourse]
		PRIMARY KEY
		CLUSTERED
		([CrossReferenceID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndCourse_ContentSurveyQuestions]
	FOREIGN KEY ([ContentID]) REFERENCES [dbo].[Content] ([ContentID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndCourse_ContentSurveyQuestions]

GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndCourse_CrossReferenceSurveyAndCourse]
	FOREIGN KEY ([ActionID]) REFERENCES [dbo].[Actions] ([ActionID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndCourse_CrossReferenceSurveyAndCourse]

GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	WITH CHECK
	ADD CONSTRAINT [FK_CrossReferenceSurveyAndCourse_SpecialForces]
	FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID])
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse]
	CHECK CONSTRAINT [FK_CrossReferenceSurveyAndCourse_SpecialForces]

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CrossReferenceSurveyAndCourse]
	ON [dbo].[CrossReferenceSurveyAndCourse] ([ContentID], [QuestionID], [AnswerID], [CourseID], [ActionID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[CrossReferenceSurveyAndCourse] SET (LOCK_ESCALATION = TABLE)
GO
