SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[UserHug] (
		[HugID]              [int] IDENTITY(1, 1) NOT NULL,
		[UserID]             [int] NOT NULL,
		[NameHugID]          [int] NOT NULL,
		[LastUpdateDate]     [datetime] NULL,
		CONSTRAINT [PK_UserHug]
		PRIMARY KEY
		CLUSTERED
		([UserID], [NameHugID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserHug] SET (LOCK_ESCALATION = TABLE)
GO
