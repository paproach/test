SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScorePoints] (
		[ActionTypeID]       [int] IDENTITY(1, 1) NOT NULL,
		[ActionTypeDesc]     [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		[BonusPoints]        [float] NULL,
		[OneTime]            [bit] NULL,
		[Journal]            [bit] NULL,
		CONSTRAINT [PK_TblAuditActionTypes]
		PRIMARY KEY
		CLUSTERED
		([ActionTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScorePoints]
	ADD
	CONSTRAINT [DF_AuditActionTypes_OneTime]
	DEFAULT ((0)) FOR [OneTime]
GO
ALTER TABLE [dbo].[ScorePoints] SET (LOCK_ESCALATION = TABLE)
GO
