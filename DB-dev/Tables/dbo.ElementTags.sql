SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ElementTags] (
		[ElementTypeID]     [int] NOT NULL,
		[ElementID]         [int] NOT NULL,
		[TagID]             [int] NOT NULL,
		[IsHidden]          [bit] NULL,
		CONSTRAINT [PK_TblElementTags]
		PRIMARY KEY
		CLUSTERED
		([ElementTypeID], [ElementID], [TagID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementTags] SET (LOCK_ESCALATION = TABLE)
GO
