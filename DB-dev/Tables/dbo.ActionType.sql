SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionType] (
		[ActionTypeID]             [int] NOT NULL,
		[ActionTypeDefinition]     [nvarchar](200) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_ActionType]
		PRIMARY KEY
		CLUSTERED
		([ActionTypeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionType] SET (LOCK_ESCALATION = TABLE)
GO
