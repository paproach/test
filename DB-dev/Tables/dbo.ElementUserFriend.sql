SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElementUserFriend] (
		[UserID]            [int] NOT NULL,
		[FriendID]          [int] NOT NULL,
		[SocialNetwork]     [nvarchar](50) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [PK_ElementUserFriend_1]
		PRIMARY KEY
		CLUSTERED
		([UserID], [FriendID], [SocialNetwork])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ElementUserFriend]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementUserFriend_UserFriend]
	FOREIGN KEY ([FriendID]) REFERENCES [dbo].[UserFriend] ([UserFriendID])
ALTER TABLE [dbo].[ElementUserFriend]
	CHECK CONSTRAINT [FK_ElementUserFriend_UserFriend]

GO
ALTER TABLE [dbo].[ElementUserFriend]
	WITH CHECK
	ADD CONSTRAINT [FK_ElementUserFriend_Users]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[ElementUserFriend]
	CHECK CONSTRAINT [FK_ElementUserFriend_Users]

GO
ALTER TABLE [dbo].[ElementUserFriend] SET (LOCK_ESCALATION = TABLE)
GO
