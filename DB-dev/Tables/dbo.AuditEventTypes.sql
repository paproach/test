SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditEventTypes] (
		[AuditEventName]     [nvarchar](50) COLLATE Hebrew_CI_AS NOT NULL,
		[AuditEventDesc]     [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		CONSTRAINT [PK_TblAuditActions]
		PRIMARY KEY
		CLUSTERED
		([AuditEventName])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditEventTypes] SET (LOCK_ESCALATION = TABLE)
GO
