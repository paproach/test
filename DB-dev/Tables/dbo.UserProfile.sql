SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile] (
		[UserId]       [int] IDENTITY(1, 1) NOT NULL,
		[UserName]     [nvarchar](56) COLLATE Hebrew_CI_AS NOT NULL,
		CONSTRAINT [UQ__UserProf__C9F284563B4BBA2E]
		UNIQUE
		NONCLUSTERED
		([UserName])
		ON [PRIMARY],
		CONSTRAINT [PK__UserProf__1788CC4C386F4D83]
		PRIMARY KEY
		CLUSTERED
		([UserId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserProfile] SET (LOCK_ESCALATION = TABLE)
GO
