SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comments] (
		[CommentID]           [int] IDENTITY(1, 1) NOT NULL,
		[ElementTypeID]       [int] NOT NULL,
		[ElementID]           [int] NOT NULL,
		[Title]               [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Comment]             [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[DatePublished]       [datetime] NULL,
		[ParentCommentID]     [int] NULL,
		[UserID]              [int] NULL,
		[viewCount]           [int] NULL,
		[HugsCount]           [int] NULL,
		[StatusApproval]      [int] NULL,
		[UserAnonymous]       [bit] NULL,
		CONSTRAINT [PK_TblComments]
		PRIMARY KEY
		CLUSTERED
		([CommentID], [ElementTypeID], [ElementID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comments]
	ADD
	CONSTRAINT [DF_Comments_DatePublished]
	DEFAULT (((1)-(1))-(1900)) FOR [DatePublished]
GO
ALTER TABLE [dbo].[Comments]
	ADD
	CONSTRAINT [DF_Comments_ParentCommentID]
	DEFAULT ((1)) FOR [ParentCommentID]
GO
ALTER TABLE [dbo].[Comments]
	ADD
	CONSTRAINT [DF_Comments_UserID]
	DEFAULT ((1)) FOR [UserID]
GO
ALTER TABLE [dbo].[Comments]
	ADD
	CONSTRAINT [DF_Comments_viewCount]
	DEFAULT ((1)) FOR [viewCount]
GO
ALTER TABLE [dbo].[Comments]
	WITH CHECK
	ADD CONSTRAINT [FK_Comments_ApprovalStatus]
	FOREIGN KEY ([StatusApproval]) REFERENCES [dbo].[ApprovalStatus] ([StatusID])
ALTER TABLE [dbo].[Comments]
	CHECK CONSTRAINT [FK_Comments_ApprovalStatus]

GO
ALTER TABLE [dbo].[Comments]
	WITH CHECK
	ADD CONSTRAINT [FK_TblComments_TblUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
ALTER TABLE [dbo].[Comments]
	CHECK CONSTRAINT [FK_TblComments_TblUsers]

GO
ALTER TABLE [dbo].[Comments] SET (LOCK_ESCALATION = TABLE)
GO
