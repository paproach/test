SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscriptions] (
		[SubscriptionID]       [int] IDENTITY(1, 1) NOT NULL,
		[Position]             [int] NULL,
		[Title]                [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[SubTitle]             [nvarchar](50) COLLATE Hebrew_CI_AS NULL,
		[IsSinglePayment]      [bit] NULL,
		[TotalPrice]           [float] NULL,
		[MonthlyPrice]         [float] NULL,
		[NumberOfPayments]     [int] NULL,
		[DurationInMonths]     [int] NULL,
		CONSTRAINT [PK_Subscriptions]
		PRIMARY KEY
		CLUSTERED
		([SubscriptionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Subscriptions] SET (LOCK_ESCALATION = TABLE)
GO
