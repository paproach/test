SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionQuestionAnswers] (
		[QuestionID]           [int] NOT NULL,
		[PossibleAnswerID]     [int] IDENTITY(1, 1) NOT NULL,
		[PossibleAnswer]       [nvarchar](250) COLLATE Hebrew_CI_AS NULL,
		[AnswerScore]          [bit] NULL,
		CONSTRAINT [PK_SpecialForceActionQuestionAnswers]
		PRIMARY KEY
		CLUSTERED
		([QuestionID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionQuestionAnswers]
	WITH CHECK
	ADD CONSTRAINT [FK_SpecialForceActionQuestionAnswers_SpecialForceActionQuestions]
	FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[ActionQuestions] ([QuestionID])
ALTER TABLE [dbo].[ActionQuestionAnswers]
	CHECK CONSTRAINT [FK_SpecialForceActionQuestionAnswers_SpecialForceActionQuestions]

GO
ALTER TABLE [dbo].[ActionQuestionAnswers] SET (LOCK_ESCALATION = TABLE)
GO
