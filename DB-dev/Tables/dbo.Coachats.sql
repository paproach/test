SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coachats] (
		[CoachatID]                     [int] IDENTITY(1, 1) NOT NULL,
		[Name]                          [nvarchar](100) COLLATE Hebrew_CI_AS NULL,
		[Description]                   [nvarchar](max) COLLATE Hebrew_CI_AS NULL,
		[TriggerID]                     [int] NULL,
		[RelevantToCommunityID]         [int] NULL,
		[RelevantToCourseID]            [int] NULL,
		[MinExtraversionRate]           [float] NULL,
		[MaxExtraversionRate]           [float] NULL,
		[MinTraitAgreeableness]         [float] NULL,
		[MaxTraitAgreeableness]         [float] NULL,
		[MinTraitConscientiousness]     [float] NULL,
		[MaxTraitConscientiousness]     [float] NULL,
		[MinTraitNeuroticism]           [float] NULL,
		[MaxTraitNeuroticism]           [float] NULL,
		[MaxTraitOpenness]              [float] NULL,
		[MinTraitOpenness]              [float] NULL,
		[IsEnabled]                     [bit] NULL,
		[StartingStepID]                [int] NULL,
		[CoachatIconID]                 [int] NULL,
		[IsMultipageCoachat]            [bit] NULL,
		CONSTRAINT [PK_Coachats]
		PRIMARY KEY
		CLUSTERED
		([CoachatID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Coachats]
	WITH CHECK
	ADD CONSTRAINT [FK_Coachats_CoachatIcons]
	FOREIGN KEY ([CoachatIconID]) REFERENCES [dbo].[CoachatIcons] ([CoachatIconID])
ALTER TABLE [dbo].[Coachats]
	CHECK CONSTRAINT [FK_Coachats_CoachatIcons]

GO
ALTER TABLE [dbo].[Coachats]
	WITH CHECK
	ADD CONSTRAINT [FK_Coachats_Triggers]
	FOREIGN KEY ([TriggerID]) REFERENCES [dbo].[Triggers] ([TriggerID])
ALTER TABLE [dbo].[Coachats]
	CHECK CONSTRAINT [FK_Coachats_Triggers]

GO
ALTER TABLE [dbo].[Coachats] SET (LOCK_ESCALATION = TABLE)
GO
